import React, { useEffect, useRef, useState } from 'react'
import { donHangService } from '../../services/donHang/donHangService'
import { sessionService } from '../../services/sessionService/SessionService';
import { userService} from "../../services/userService/UserService"
import icon_add from "../../isset/img/icon/ICON ADD.png"
import icon_edit from "../../isset/img/icon/Báo cáo tổng-01.png"
import icon_delete from "../../isset/img/icon/ICON REMOVE.png"
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { Popconfirm } from 'antd';
import moment from 'moment/moment';

export default function DonHang() {
    const inputEl = useRef(null);
    let user = sessionService.getUser();
    let access_page = JSON.parse(user.access_page);
    let action_page = JSON.parse(user.action_page);
    let today = moment().format("YYYY-MM");
    let [day1,setDay1] = useState(today + "-01");
    let [day2,setDay2] = useState(today + "-31");
    let [orderArray,setOrderArray] = useState([]);
    let [serviceArray,setServiceArray] = useState([]);
    let [cloneOrder,setCloneOrder] = useState([]);
    let [thoArray,setThoArray] = useState([]);
    let [khachArray,setKhachArray] = useState([]);
    let [modal,setModal] = useState({});
    let [isUpdate,setIsUpdate] = useState(false);
  useEffect(() => {
          donHangService.getDonHang(day1,day2,user.token).then((res) => {
            setOrderArray(res.data);
            setCloneOrder(res.data);
          })
          .catch((err) => {
           console.log(err);
          });
          donHangService.getDichVu(user.token).then((res) => {
                  setServiceArray(res.data);
                })
                .catch((err) => {
                 console.log(err);
                });
        userService.getUserKhach(user.token).then((res) => {
                setKhachArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
        userService.getUserTho(user.token).then((res) => {
                setThoArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
  },[])
    
  let sortArray = (e) => {
    let array = cloneOrder.slice()
    if(e.target.value == 0){
      array.sort(function(a,b){
        var nameA = a.order_create_date;
        var nameB = b.order_create_date;
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // name trùng nhau
        return 0;
      });
    }else{
      array.sort(function(a,b){
        var nameA = a.order_create_date;
        var nameB = b.order_create_date;
        if (nameA > nameB) {
          return -1;
        }
        if (nameA < nameB) {
          return 1;
        }

        // name trùng nhau
        return 0;
      });
    }
    setOrderArray(array);
  }
    let changeFilter = (e) => {
        let clone = [];
        if(e.target.value == -1){
          setOrderArray(cloneOrder);
        }else {
          cloneOrder.map((order) => {
            if(order.order_status == e.target.value){
              clone.push(order);
            }
            setOrderArray(clone);
          })
        }
      }
    let handleSetModal = (id) => {
      if(id == -1){
        let tmpModal = {
          order_code: "",
          order_date:"",
          order_time:"",
          order_price:"",
          order_cus_id:0,
          order_address:"",
          order_status: 0,
          order_tho_id:0,
          order_ly_do:"",
          service_id:1,
          order_desc:""
        }
        setModal(tmpModal);
        setIsUpdate(false);
      }else{
        let order_date = moment(orderArray[id].order_working_date).format("YYYY-MM-DD");
        let order_time = moment(orderArray[id].order_working_date).format("HH:mm:ss");
        setModal({...orderArray[id],order_date,order_time});
        setIsUpdate(true);
      }
    }
    let changeModal = (e) => {
      modal[e.target.name] = e.target.value;
      if(e.target.name == "service_id"){
        let index = serviceArray.findIndex((service) => {
          return service.service_id == e.target.value;
        })
        document.getElementById("order_price").value = serviceArray[index].service_price;
        modal.order_price = serviceArray[index].service_price;
      }
    }
      let handleCheckCode = () => {
        donHangService.checkMaDon(user.token,inputEl.current.value).then((res) => {
                  setOrderArray([res.data]);
                })
                .catch((err) => {
                  toast.error("Vui lòng kiểm tra lại mã đơn!",{position: toast.POSITION.TOP_CENTER});
                });
      }
      const randomString = () => {
        const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let result = '';
        for (let i = 6; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
      }
      let handleCreateUpdate = () => {
      if(modal.order_code == "" || modal.order_code == null){
        modal.order_code = randomString();
      }
      modal.order_price = modal.order_price.toString();
      modal.order_price = modal.order_price.split(".").join("");
      modal.order_price = modal.order_price.split(",").join("");
      modal.order_price = modal.order_price.split(" ").join("");
      modal.order_cus_id = Number(modal.order_cus_id);
      modal.order_tho_id = Number(modal.order_tho_id);
      modal.order_status = Number(modal.order_status);
      modal.service_id = Number(modal.service_id);
      if(modal.order_cus_id == 0){
        toast.error("Vui lòng Chọn Khách Đặt",{position: toast.POSITION.TOP_CENTER});
        return;
      }
      if(modal.order_date == ""){
        toast.error("Vui lòng Chọn Ngày Thực Hiện",{position: toast.POSITION.TOP_CENTER});
        return;
      }
      if(modal.order_time == ""){
        toast.error("Vui lòng Chọn Giờ Thực Hiện",{position: toast.POSITION.TOP_CENTER});
        return;
      }
      if(modal.order_price == ""){
        toast.error("Vui lòng Nhập Giá Tiền",{position: toast.POSITION.TOP_CENTER});
        return;
      }
      if(modal.order_address.length <= 5){
        toast.error("Vui lòng Nhập Địa Chỉ",{position: toast.POSITION.TOP_CENTER});
        return;
      };
      if(isUpdate){
        modal.order_working_date = modal.order_date + " " + modal.order_time + ":00";
        delete modal.order_date; delete modal.order_time; delete modal.images;
        delete modal.user_app_khach; delete modal.user_app_tho; delete modal.danh_gia;
        delete modal.tien_do; delete modal.services;
        donHangService.updateDonHang(modal,user.token).then((res) => {
          toast.success("Cập nhật đơn hàng thành công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
              })
              .catch((err) => {
                toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
              });
      }else{
        modal.order_working_date = modal.order_date + " " + modal.order_time + ":00";
        delete modal.order_date; delete modal.order_time; delete modal.images;
        donHangService.createDonHang(modal,user.token).then((res) => {
          toast.success("Tạo đơn hàng thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
        })
        .catch((err) => {
          toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
        });
      }
    }
    let handleDeleteService = (id) => {
      donHangService.updateDonHang({order_id:id,order_status:3},user.token).then((res) => {
          toast.success("Xoá dịch vụ thành công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
            })
            .catch((err) => {
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }
    let handleDeleteImage = (id) => {
      donHangService.deleteImage(id,user.token).then((res) => {
        toast.success("Xoá hình ảnh thành công",{position: toast.POSITION.TOP_CENTER});
        window.location.reload();
            })
            .catch((err) => {
             console.log(err);
            });
    }
    let handleUploadAnh = (order_id) => {
      let files = document.getElementById("order_img").files;
      donHangService.uploadImage(files,order_id,user.token).then((res) => {
        toast.success("Upload Hình Thành Công",{position: toast.POSITION.TOP_CENTER});
        window.location.reload();
            })
            .catch((err) => {
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }
    let handleUpdateFile = (order_id) => {
      let files = document.getElementsByName("order_file")[0].files[0];
      donHangService.updateFile(files,order_id,user.token).then((res) => {
        toast.success("Upload File Thành Công",{position: toast.POSITION.TOP_CENTER});
        window.location.reload();
            })
            .catch((err) => {
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }
    let handleSearch = () => {
      donHangService.getDonHang(day1,day2,user.token).then((res) => {
              setOrderArray(res.data);
              setCloneOrder(res.data);
            })
            .catch((err) => {
             console.log(err);
            });
  }
    let renderSelectDichVu = (service_id) => {
        return serviceArray?.map((service) => {
          if(service.service_id == service_id){
            return <option selected value={service.service_id}>{service.service_name}</option>
          }else{
            return <option value={service.service_id}>{service.service_name}</option>
          }
        })
    }
    let renderSelectKhach = (user_id) => {
      return khachArray?.map((khach) => {
        if(khach.user_id == user_id){
          return <option selected value={khach.user_id}>{khach.user_username} : {khach.user_fullname}</option>
        }else{
          return <option value={khach.user_id}>{khach.user_username} : {khach.user_fullname}</option>
        }
      })
    }
    let renderSelectTho = (user_id) => {
      thoArray = [{user_id:0,user_fullname:"Chưa Có Thợ Nhận"},...thoArray];
      return thoArray?.map((tho) => {
        if(tho.user_id == user_id){
          return <option selected value={tho.user_id}>{tho.user_username} : {tho.user_fullname}</option>
        }else{
          return <option value={tho.user_id}>{tho.user_username} : {tho.user_fullname}</option>
        }
      })
    }
    let renderStatus = (status) => {
      if(status == 0){
        return <td style={{background:"gray"}} className='text-light'>Chưa xác nhận</td>
      }
      if(status == 1){
        return <td style={{background:"yellow"}} >Đang Thực Hiện</td>
      }
      if(status == 2){
        return <td style={{background:"green"}} className='text-light'>Đã Hoàn Thành</td>
      }
      if(status == 3){
        return <td style={{background:"red"}} className='text-light'>Đã Huỷ</td>
      }
    }
    let renderSelectStatus = (status) => {
      let status_array = ["Chờ Duyệt","Đang Thực Hiện","Đã Hoàn Thành","Đã Huỷ"];
      return status_array.map((item,index) => {
        if(index == status){
          return <option selected value={index}>{status_array[index]}</option>
        }else{
          return <option value={index}>{status_array[index]}</option>
        }
      })
    }
    let renderThanhToan = (status,gate) => {
      if(status == 0){
        return <p>Chưa Thanh Toán</p>
      }else{
        return <p>{gate}</p>
        }
    }
    let renderAddButton = () => {
      if(action_page[2]?.action_create == 1){
        return <button onClick={() => handleSetModal(-1)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0"}}>
          <img width={"20px"} height={"20px"} src={icon_add} alt="" />
        </button>
      }
    }
    let renderEditButton = (id) => {
      if(action_page[2].action_update == 1){
        return <button onClick={() => handleSetModal(id)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0",background: "transparent",marginRight:"7px"}}>
          <img width={"20px"} height={"20px"} src={icon_edit} alt="" />
        </button>
      }
    }
    let renderDeleteButton = (id) => {
      if(action_page[3].action_delete == 1){
        return <Popconfirm
        title="Xoá ĐƠn Hàng"
        description="Bạn Chắc chắn xoá đơn hàng này?"
        okText="Xoá"
        cancelText="Huỷ"
        onConfirm={() => {handleDeleteService(id)}}
      >
        <button style={{border:"none",padding:"0",background: "transparent"}}>
          <img width={"20px"} height={"20px"} src={icon_delete} alt="" />
        </button>
      </Popconfirm>
      }
    }
    let setDaySelect = (e) => {
      if(e.target.name == "day1" ){
          setDay1(e.target.value);
      };
      if(e.target.name == "day2"){
          setDay2(e.target.value);
      }
  }
    let renderSelect = () => {
      return <div className='text-start mb-3'>
          <span className='me-2'>Từ</span>
          <input onChange={setDaySelect} defaultValue={day1} type="date" name='day1' />
          <span className='m-2'>Đến</span>
          <input onChange={setDaySelect} defaultValue={day2} type="date" name='day2' />
          <button onClick={handleSearch} style={{background:"#3498db"}} className='ms-2 border-0 rounded-2 px-2 py-1 text-light'>Lọc</button>
      </div>
  }
    let renderDonHangItem = () => {
      return orderArray.map((order,index) => {
        return(
          <tr>
            <td><p>{index + 1}</p></td>
            <td>{order.order_code}</td>
            <td><p>{order.services.service_name}</p></td>
            <td><button onClick={() => handleSetModal(index)} type="button" className='border-0 bg-body text-decoration-underline text-primary' data-bs-toggle="modal" data-bs-target="#modalUpload">Quản Lý</button></td>
            <td><p>{order.order_desc}</p></td>
            <td><p>{moment(order.order_create_date).format("HH:mm:ss DD-MM-YYYY")}</p></td>
            <td><p>{moment(order.order_working_date).format("HH:mm:ss DD-MM-YYYY")}</p></td>
            <td><p>{new Intl.NumberFormat('en-US').format(order.order_price)}</p></td>
            <td><p>{order.user_app_khach.user_username}:{order.user_app_khach.user_fullname}</p></td>
            <td><p>{order.user_app_khach.user_phone}</p></td>
            <td><p>{order.order_address}</p></td>
            {renderStatus(order.order_status)}
            <td><p>{order.order_finish_date?moment(order.order_finish_date).format("HH:mm:ss DD-MM-YYYY"):""}</p></td>
            <td><p>{order.user_app_tho?.user_username}:{order.user_app_tho?.user_fullname}</p></td>
            <td><button onClick={() => handleSetModal(index)} type="button" className='border-0 bg-body text-decoration-underline text-primary' data-bs-toggle="modal" data-bs-target="#modalTienDo">Xem</button></td>
            <td><button onClick={() => handleSetModal(index)} type="button" className='border-0 bg-body text-decoration-underline text-primary' data-bs-toggle="modal" data-bs-target="#modalFile">Quản Lý</button></td>
            <td><p>{order.order_ly_do}</p></td>
            <td><p>{order.order_source}</p></td>
            <td>{renderThanhToan(order.order_payment_status,order.order_payment_gate)}</td>
            <td>{order.order_payment_img1?<a href={order.order_payment_img1} target="_blank" rel="noopener noreferrer">Xem</a>:""}</td>
            <td>{order.order_payment_img2?<a href={order.order_payment_img2} target="_blank" rel="noopener noreferrer">Xem</a>:""}</td>
            <td>{renderEditButton(index)}{renderDeleteButton(order.order_id)}</td>
          </tr>
        )
      }) 
    }
    let renderDonHang = () => {
      if(access_page[2].access_status != 1){
        return(
          <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
        )
      }else{
        return(
          <>
            <table class="table table-bordered">
              <thead>
                <th style={{width:"40px"}}>{renderAddButton()}</th>
                <th>Mã Đơn</th>
                <th>Tên Đơn Hàng</th>
                <th>Hình Ảnh</th>
                <th>Chi Tiết</th>
                <th>Thời Gian Đặt</th>
                <th>Thời Gian Thực Hiện</th>
                <th>Giá Đơn Hàng</th>
                <th>Tài Khoản Đặt</th>
                <th>Số Điện Thoại</th>
                <th>Địa Chỉ</th>
                <th>Trạng Thái</th>
                <th>Thời Gian Hoàn Thành</th>
                <th>Thợ Nhận</th>
                <th>Tiến Trình</th>
                <th>File</th>
                <th>Lý Do Huỷ Đơn</th>
                <th>Nguồn Nhận Đơn</th>
                <th>Thanh Toán</th>
                <th>Hình Thanh Toán 1</th>
                <th>Hình Thanh Toán 2</th>
                <th style={{width:"90px"}}>Thao Tác</th>
              </thead>
              <tbody id='myTable'>
                  {renderDonHangItem()}
              </tbody>
            </table>
          </>
        )
      }
    }
    let renderModal = () => {
      return(
        <div class="modal fade modal-xl" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div style={{background:"#037ec1"}} class="modal-header">
                <div></div>
                <h1 class="modal-title fs-2 text-light" id="exampleModalLabel">Thêm Đơn Hàng</h1>
                <button style={{marginLeft:"0"}} type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <div className='row'>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Chọn Dịch Vụ</label>
                      <div>
                        <select onChange={changeModal} id="service_id" class="form-control input-lg" name="service_id" >{renderSelectDichVu(modal.service_id)}</select>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Mô Tả Đơn Hàng</label>
                      <div>
                        <input onChange={changeModal} type="text" id="order_desc" class="form-control input-lg" name="order_desc" defaultValue={modal.order_desc} placeholder="Nhập mô tả đơn hàng"></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Thời Gian Thực Hiện</label>
                      <div>
                        <input defaultValue={modal.order_date} onChange={changeModal} type="date" id="order_date" class="form-control input-lg" name="order_date"  placeholder="Nhập Ngày Thực Hiện"></input>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='row mt-3'>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Thời Gian Thực Hiện</label>
                      <div>
                        <input defaultValue={modal.order_time} onChange={changeModal} type="time" id="order_time" class="form-control input-lg" name="order_time"  placeholder="Nhập Giờ Thực Hiện"></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Giá Tiền</label>
                      <div>
                      <input onChange={changeModal} type="text" id="order_price" class="form-control input-lg" name="order_price" defaultValue={modal.order_price}  placeholder="Nhập Giá Tiền"></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Khách Đặt Đơn</label>
                      <div>
                      <select onChange={changeModal} id="order_cus_id" class="form-control input-lg" name="order_cus_id">
                          {renderSelectKhach(modal.order_cus_id)}
                      </select>
                      </div>
                    </div>
                  </div>
                </div>
                {/* Hàng 3 */}
                <div className='row mt-3'>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Địa Chỉ Thực Hiện</label>
                      <div>
                        <input defaultValue={modal.order_address} onChange={changeModal} type="text" id="order_address" class="form-control input-lg" name="order_address"  placeholder="Nhập Địa Chỉ"></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Trạng Thái</label>
                      <div>
                        <select onChange={changeModal} id="order_status" class="form-control input-lg" name="order_status" >{renderSelectStatus(modal.order_status)}</select>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Thợ Nhận Đơn</label>
                      <div>
                      <select onChange={changeModal} id="order_tho_id" class="form-control input-lg" name="order_tho_id">
                          {renderSelectTho(modal.order_tho_id)}
                      </select>
                      </div>
                    </div>
                  </div>
                </div>
                {/* Hàng 4 */}
                <div className='row mt-3'>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Lý Do Huỷ Đơn</label>
                      <div>
                        <input onChange={changeModal} type="text" id="order_ly_do" class="form-control input-lg" name="order_ly_do"  placeholder="Nhập Lý Do Huỷ"></input>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                <button onClick={handleCreateUpdate} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
              </div>
            </div>
          </div>
        </div>
      )
    }
    let renderImg = (type) => {
      if(type == "order"){
        return modal.images?.map((img) => {
          return <div className='col-6'>
            <div>
              <a target="_blank" href={img?.img_path}><img src={img?.img_path} width={"90%"} alt="" /></a>
            </div>
              <button onClick={() => handleDeleteImage(img?.img_id)} className='bg-danger text-light mt-2 border-0'>Xoá</button>
            </div>
        })
      }
    }
    let renderModalUpload = () => {
      return(
        <div class="modal fade modal-xl" id="modalUpload" tabindex="-1" aria-labelledby="modalUploadLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                
                <h1 class="modal-title fs-5" id="modalUploadLabel">Quản Lý Ảnh</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                  <div className='row'>
                    {renderImg("order")}
                  </div>
                  <div className='upload mt-3'>
                    <h5>Thay Đổi Ảnh</h5>
                    <input className="form-control" multiple
                        type="file" accept='image/*'
                        id='order_img'
                        name = "order_img"
                        />
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                <button onClick={() => handleUploadAnh(modal.order_id)} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
              </div>
            </div>
          </div>
        </div>
      )
    }
    let renderTienDo = () => {
      return modal.tien_do?.map((tienDo) => {
          return <div className='mb-2' style={{border:"1px solid black"}}>
            <h4 className='text-success'>{tienDo.tiendo_name}</h4>
            <p className='text-danger'><i class="fa fa-clock me-3"></i>{moment(tienDo.tiendo_date).format("HH:mm:ss DD/MM/YYYY")}</p>
            <div className='row'>
                <div className='col-6'>
                  <a target="_blank" href={tienDo.tiendo_img1}><img src={tienDo.tiendo_img1} width={"80%"} alt="" /></a>
                </div>
                <div className='col-6'>
                  <a target="_blank" href={tienDo.tiendo_img2}><img src={tienDo.tiendo_img2} width={"80%"} alt="" /></a>
                </div>
            </div>
            <p style={{border:"1px dotted gray"}} className="mt-3 p-2">{tienDo.tiendo_desc}</p>
          </div>
      })
    }
    let renderModalTienDo = () => {
      return(
        <div class="modal fade modal-xl" id="modalTienDo" tabindex="-1" aria-labelledby="modalTienDoLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                
                <h1 class="modal-title fs-5" id="modalTienDoLabel">Xem Tiến Độ</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                    {renderTienDo()}
              </div>
            </div>
          </div>
        </div>
      )
    }
    let renderModalFile = () => {
      return(
        <div class="modal fade" id="modalFile" tabindex="-1" aria-labelledby="modalFileLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                
                <h1 class="modal-title fs-5" id="modalFileLabel">Xem File</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                    <div className='row'>
                      {modal.order_file?<a target='_blank' href={modal.order_file}>Tải về</a>:""}
                    </div>
                    <div className='upload mt-3'>
                      <h5>Thay Đổi File</h5>
                      <div className='mt-3'>
                        <input className="form-control"
                          type="file"
                          name = "order_file"
                          />
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                      <button onClick={() => handleUpdateFile(modal.order_id)} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
                  </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
  return (
    <div id='donHang' className=''>
        <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Danh Sách Đơn Hàng</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input autoComplete='off' placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i className="fas fa-search"></i>
            </button>
            <select onChange={sortArray} style={{marginLeft:"20px",borderRadius:"5px",borderColor:"#d5d5d5"}} name="" id="">
              <option selected value="0">Ngày Tạo Đơn Xa Nhất</option>
              <option value="1">Ngày Tạo Đơn Gần Nhất</option>
            </select>
            <div style={{width:"30%"}} class="form-outline d-flex">
              <label className='align-self-center ms-5 me-2 fw-bolder'>Chọn Trạng Thái:</label>
              <select onChange={changeFilter} style={{border:"1px solid #dddddd"}} className='w-50 h-100' name="" id="">
                <option value="-1">Tất Cả</option>
                <option value="0">Chưa Xác Nhận</option>
                <option value="1">Đang Thực Hiện</option>
                <option value="2">Đã Hoàn Thành</option>
                <option value="3">Đã Huỷ</option>
              </select>
            </div>
            <div style={{width:"30%"}} className='form-outline d-flex'>
              <label style={{width:"50%"}} className='align-self-center ms-5 me-2 fw-bolder'>Mã đơn:</label>
              <input ref={inputEl} placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
              <button onClick={handleCheckCode} style={{borderRadius: "0px"}} type="button" class="btn btn-primary">
                <i className="fas fa-search"></i>
              </button>
            </div>
            
          </div>
          {renderSelect()}
          {renderDonHang()}
          {renderModal()}
          {renderModalUpload()}
          {renderModalTienDo()}
          {renderModalFile()}
        </div>
        <ToastContainer></ToastContainer>
    </div>
  )
}
