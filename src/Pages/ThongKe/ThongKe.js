import React, { useEffect, useRef, useState } from 'react'
import { thongKeService } from '../../services/thongKe/thongKeService';
import {appThoService} from "../../services/appThoService/appThoService";
import { sessionService } from '../../services/sessionService/SessionService';
import moment from 'moment/moment';
import { DownloadTableExcel } from 'react-export-table-to-excel';
import icon_excel from '../../isset/img/icon/excel.png'
import { dichVuService } from '../../services/dichVuService/dichVuService';

export default function ThongKe() {
        let user = sessionService.getUser();
        let access_page = JSON.parse(user.access_page);
        let today = moment().format("YYYY-MM-DD");
        let [day1,setDay1] = useState(moment(today).format("YYYY-MM") + "-01");
        let [day2,setDay2] = useState(today);
        let [thoAray,setThoArray] = useState([]);
        let [chietKhau,setChietKhau] = useState(10);
        let [dichVuArray,setDichVuArray] = useState([]);
        let tong = 0;
        let tongCk = 0;
        let tongThucNhan = 0;
        const tableRef = useRef(null);
        const tableRef2 = useRef(null);
    useEffect(() => {
        thongKeService.getDonHang(user.token,day1,day2).then((res) => {
                setOrderArray(res.data);
                setCloneOrder(res.data);
                setChietKhauArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
        appThoService.getUserList(user.token).then((res) => {
                setThoArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
        dichVuService.getDichVu(user.token).then((res) => {
                console.log(res.data);
                setDichVuArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
    },[]);
    let [orderArray,setOrderArray] = useState([]);
    let [chietKhauArray,setChietKhauArray] = useState([]);
    let [cloneOrder,setCloneOrder] = useState([]);
    let changeFilter = (e) => {
      let clone = [];
      if(e.target.value == -1){
        setOrderArray(cloneOrder);
      }else {
        cloneOrder.map((order) => {
          if(order.order_payment_status == e.target.value){
            clone.push(order);
            setOrderArray(clone);
          }
        })
      }
  }
    let changeSelectTho = (e) => {
        let clone = [];
        if(e.target.value == 0){
          setChietKhauArray(cloneOrder);
        }else {
          cloneOrder.map((order) => {
            if(order.order_tho_id == e.target.value){
              clone.push(order);
            }
            setChietKhauArray(clone);
          })
        }
    }
    let changeSelectDichVu = (e) => {
      let clone = [];
      if(e.target.value == 0){
        setChietKhauArray(cloneOrder);
      }else {
        cloneOrder.map((order) => {
          if(order.service_id == e.target.value){
            clone.push(order);
          }
          setChietKhauArray(clone);
        })
      }
  }
    let changeChietKhau = (e) => {
      setChietKhau(e.target.value);
    }
    let renderSelectTho = () => {
      return thoAray.map((tho) => {
        return <option value={tho.user_id}>{tho.user_username} : {tho.user_fullname}</option>
      })
    }
    let renderSelectDichVu = () => {
      return dichVuArray.map((dichVu) => {
        return <option value={dichVu.service_id}>{dichVu.service_name}</option>
      })
    }
    let renderThucNhan = (donGia) => {
      let ck = Number(chietKhau);
      let thucNhan = donGia - (donGia*ck/100);
      tongThucNhan += thucNhan;
      return new Intl.NumberFormat('en-US').format(thucNhan)
    }
    let renderDonHangItem = () => {
        return orderArray?.map((order,index) => {
            return <tr>
                <td>{index+1}</td>
                <td>{order.order_code}</td>
                <td>{order.services.service_name}</td>
                <td>{order.order_desc}</td>
                <td>{moment(order.order_create_date).format("HH:mm:ss DD/MM/YYYY")}</td>
                <td>{moment(order.order_working_date).format("HH:mm:ss DD/MM/YYYY")}</td>
                <td>{moment(order.order_finish_date).format("HH:mm:ss DD/MM/YYYY")}</td>
                <td>{new Intl.NumberFormat('en-US').format(order.order_price)}</td>
                <td>{new Intl.NumberFormat('en-US').format(order.so_tien_giam)}</td>
                <td>{new Intl.NumberFormat('en-US').format(order.gia_da_giam)}</td>
                <td>{order.promo_code}</td>
                <td>{order.order_payment_gate}</td>
                {order.order_payment_status == 1?<td style={{background:"green",color:"white"}}>Đã Thanh Toán</td>:<td style={{background:"yellow"}}>Chưa Thanh Toán</td>}
            </tr>
        })
    }
    let renderChietKhauItem = () => {
      return chietKhauArray?.map((order,index) => {
        return <tr>
          <td>{index+1}</td>
          <td>{order.order_code}</td>
          <td>{order.services.service_name}</td>
          <td>{order?.user_app_tho?.user_username} : {order?.user_app_tho?.user_fullname}</td>
          <td>{moment(order.order_finish_date).format("HH:mm:ss DD/MM/YYYY")}</td>
          <td>{new Intl.NumberFormat('en-US').format(order.order_price)}</td>
          <td>{chietKhau}%</td>
          <td>{renderThucNhan(order?.order_price)}</td>
        </tr>
      })
    }
    let tinhTongTien = () => {
        let tongGiam = 0;
        let tongThanhToan = 0;
        orderArray.forEach((order) => {
            // order.order_price = order.order_price.split(".").join("");
            // order.order_price = order.order_price.split(",").join("");
            // order.order_price = order.order_price.split(" ").join("");
            order.order_price = Number(order.order_price);
            order.gia_da_giam = Number(order.gia_da_giam);
            order.so_tien_giam = Number(order.so_tien_giam);
            tong += order.order_price;
            tongGiam += order.so_tien_giam;
            tongThanhToan += order.gia_da_giam;
        });
      return(
        <>
          <td>{new Intl.NumberFormat('en-US').format(tong)}</td>
          <td>{new Intl.NumberFormat('en-US').format(tongGiam)}</td>
          <td>{new Intl.NumberFormat('en-US').format(tongThanhToan)}</td>
        </>
      );
    }
    let tinhTongCk = () => {
        chietKhauArray.forEach((order) => {
            order.order_price = Number(order.order_price);
            tongCk += order.order_price;
        })
        return(
          <>
            <td>{new Intl.NumberFormat('en-US').format(tongCk)}</td>
            <td></td>
            <td>{new Intl.NumberFormat('en-US').format(tongThucNhan)}</td>
          </>
        )
    }
    let renderMenuChietKhau = () => {
      return <div className="input-group m-3">
        <div style={{width:"20%"}} className='d-flex'>
          <p className='fw-bolder mb-0 align-self-center me-3'>Nhập Chiết Khấu:</p><input onChange={changeChietKhau} style={{width:"70px"}} value={chietKhau} type="text" className='form-control input-lg' />
        </div>
        <div style={{width:"30%"}} className='d-flex'>
          <p className='fw-bolder mb-0 align-self-center me-3'>Chọn Thợ:</p>
            <select onChange={changeSelectTho} style={{width:"50%"}} className='form-control input-lg' name="" id="">
                    <option value="0">Tất Cả</option>
                    {renderSelectTho()}
            </select>
        </div>
        <div style={{width:"30%"}} className='d-flex'>
          <p className='fw-bolder mb-0 align-self-center me-3'>Chọn Dịch Vụ:</p>
            <select onChange={changeSelectDichVu} style={{width:"50%"}} className='form-control input-lg' name="" id="">
                    <option value="0">Tất Cả</option>
                    {renderSelectDichVu()}
            </select>
        </div>
        <div>
          <DownloadTableExcel
                    filename="thong-ke-chiet-khau"
                    sheet="chietkhau"
                    currentTableRef={tableRef2.current}
                >

                   <button style={{height:"100%",border:"none",borderRadius:"5px", padding:"5px 9px"}}> <img style={{width:"30px"}} src={icon_excel} alt="" /> Xuất Báo Cáo </button>

                </DownloadTableExcel>
        </div>
        
      </div>
    }
    let renderDonHang = () => {
        if(access_page[6].access_status == 0){
            return(
              <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
            )
          }else{
            return(
              <>
                <table class="table table-bordered" ref={tableRef}>
                  <thead>
                    <th style={{width:"40px"}}>{}</th>
                    <th>Mã Đơn</th>
                    <th>Tên Đơn Hàng</th>
                    <th>Chi Tiết</th>
                    <th>Thời Gian Tạo</th>
                    <th>Thời Gian Thực Hiện</th>
                    <th>Thời Gian Hoàn Thành</th>
                    <th>Giá Tiền</th>
                    <th>Số Tiền Giảm</th>
                    <th>Thành Tiền</th>
                    <th>Mã Giảm Giá</th>
                    <th>Phương Thức</th>
                    <th>Trạng Thái</th>
                  </thead>
                  <tbody id='myTable'>
                      {renderDonHangItem()}
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Tổng Tiền:</td>
                        {tinhTongTien()}
                        <td></td>
                        <td></td>
                      </tr>
                  </tbody>
                </table>
                <h2>Chiết Khấu Cho Thợ</h2>
                {renderMenuChietKhau()}
                <table class="table table-bordered" ref={tableRef2}>
                  <thead>
                    <th style={{width:"40px"}}>{}</th>
                    <th>Mã Đơn</th>
                    <th>Tên Đơn Hàng</th>
                    <th>Tên Thợ Thực Hiện</th>
                    <th>Thời Gian Hoàn Thành</th>
                    <th>Giá Tiền</th>
                    <th>Chiết Khấu</th>
                    <th>Thực Nhận</th>
                  </thead>
                  <tbody>
                      {renderChietKhauItem()}
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Tổng Tiền: </td>
                        {tinhTongCk()}
                      </tr>
                  </tbody>
                </table>
              </>
            )
        }
    }
    let setDaySelect = (e) => {
        if(e.target.name == "day1" ){
            setDay1(e.target.value);
        };
        if(e.target.name == "day2"){
            setDay2(e.target.value);
        }
    }
    let handleSearch = () => {
        thongKeService.getDonHang(user.token,day1,day2).then((res) => {
          console.log(res.data);
                setOrderArray(res.data);
                setCloneOrder(res.data);
                setChietKhauArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
    }
    let renderSelect = () => {
        return <div className='text-start mb-3'>
            <span className='me-2'>Từ</span>
            <input defaultValue={day1} onChange={setDaySelect} type="date" name='day1' />
            <span className='m-2'>Đến</span>
            <input defaultValue={day2} onChange={setDaySelect} type="date" name='day2' />
            <button onClick={handleSearch} style={{background:"#3498db"}} className='ms-2 border-0 rounded-2 px-2 py-1 text-light'>Lọc</button>
        </div>
    }
  return (
    <div id='thongKe'>
        <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Thống Kê Doanh Thu</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
            <div class="form-outline w-50 d-flex">
              <label className='align-self-center ms-5 me-2 fw-bolder'>Chọn Trạng Thái:</label>
              <select onChange={changeFilter} style={{border:"1px solid #dddddd"}} className='w-50 h-100' name="" id="">
                <option value="-1">Tất Cả</option>
                <option value="0">Chưa Thanh Toán</option>
                <option value="1">Đã Thanh Toán</option>
              </select>
            </div>
            <div>
              <DownloadTableExcel
                    filename="thong-ke-doanh-thu"
                    sheet="doanhthu"
                    currentTableRef={tableRef.current}
                >

                   <button style={{height:"100%",border:"none",borderRadius:"5px", padding:"5px 9px"}}> <img style={{width:"30px"}} src={icon_excel} alt="" /> Xuất Báo Cáo </button>

                </DownloadTableExcel></div>
          </div>
            {renderSelect()}
            {renderDonHang()}
        </div>
    </div>
  )
}
