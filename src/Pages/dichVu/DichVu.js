import React, { useEffect, useState } from 'react'
import {dichVuService} from "../../services/dichVuService/dichVuService"
import { sessionService } from '../../services/sessionService/SessionService';
import icon_add from "../../isset/img/icon/ICON ADD.png"
import icon_edit from "../../isset/img/icon/Báo cáo tổng-01.png"
import icon_delete from "../../isset/img/icon/ICON REMOVE.png"
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { Popconfirm } from 'antd';

export default function () {
  let user = sessionService.getUser();
  let access_page = JSON.parse(user.access_page);
  let action_page = JSON.parse(user.action_page);
    useEffect(()=>{
        dichVuService.getDichVu(user.token).then((res) => {
                setDichVuArray(res.data)
              })
              .catch((err) => {
               console.log(err);
              });
        dichVuService.getDanhMuc(user.token).then((res) => {
                setDanhMuc(res.data.content)
              })
              .catch((err) => {
               console.log(err);
              });
    },[])
    let [dichVuArray,setDichVuArray] = useState([]);
    let [modal,setModal] = useState({});
    let [isUpdate,setIsUpdate] = useState(false);
    let [danhmuc,setDanhMuc] = useState([]);
    let handleSetModal = (id) => {
      if(id == -1){
        let tmpModal = {
          service_name:"",
          service_price:"",
          service_detail:"",
          danhmuc_id:1,
          service_type:0,
          service_status: "0"
        }
        setModal(tmpModal);
        setIsUpdate(false);
      }else{
        setModal(dichVuArray[id]);
        setIsUpdate(true);
      }
    }
    let changeModal = (e) => {
      modal[e.target.name] = e.target.value;
    }
    let renderAddButton = () => {
      if(action_page[3]?.action_create == 1){
        return <button onClick={() => handleSetModal(-1)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0"}}>
          <img width={"20px"} height={"20px"} src={icon_add} alt="" />
        </button>
      }
    }
    let renderEditButton = (id) => {
      if(action_page[3].action_update == 1){
        return <button onClick={() => handleSetModal(id)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0",background: "transparent",marginRight:"7px"}}>
          <img width={"20px"} height={"20px"} src={icon_edit} alt="" />
        </button>
      }
    }
    let renderDeleteButton = (id) => {
      if(action_page[3].action_delete == 1){
        return <Popconfirm
        title="Xoá Tài Khoản"
        description="Bạn Chắc chắn xoá dịch vụ này?"
        okText="Xoá"
        cancelText="Huỷ"
        onConfirm={() => {handleDeleteService(id)}}
      >
        <button style={{border:"none",padding:"0",background: "transparent"}}>
          <img width={"20px"} height={"20px"} src={icon_delete} alt="" />
        </button>
      </Popconfirm>
      }
    }
    let renderLoaiDichVu = (type) => {
      if(type == 0){
        return "Dịch Vụ";
      }else{
        return "Gói Dịch Vụ";
      }
    }
    let renderStatus = (status) => {
      if(status == 1){
        return <p>Mở</p>
      }else{
        return <p className='text-danger'>Đóng</p>
      }
    }
    let renderSelectDanhMuc = (id) => {
      return danhmuc?.map((item,index) => {
        if(item.danhmuc_id == id){
          return <option selected value={item?.danhmuc_id}>{item?.danhmuc_ten}</option>
        }else{
          return <option value={item?.danhmuc_id}>{item?.danhmuc_ten}</option>
        }
      })
    }
    let renderSelectLoai = (type) => {
      let danhMucArray = ["Dịch Vụ","Gói Dịch Vụ"];
      return danhMucArray.map((danhMuc,index) => {
          if(index + 1 == type){
            return <option selected value={index}>{danhMuc}</option>
          }else{
            return <option value={index}>{danhMuc}</option>
          }
      })
    }
    let renderSelectStatus = (status) => {
      let status_array = ["Bị Khoá","Hoạt Động"];
      return status_array.map((item,index) => {
        if(index == status){
          return <option selected value={index}>{status_array[index]}</option>
        }else{
          return <option value={index}>{status_array[index]}</option>
        }
      })
    }
    let handleCreateUpdate = () => {
      modal.service_price = modal.service_price.split(".").join("");
      modal.service_price = modal.service_price.split(",").join("");
      modal.service_price = modal.service_price.split(" ").join("");
      modal.danhmuc_id = Number(modal.danhmuc_id);
      modal.service_status = Number(modal.service_status);
      modal.service_type = Number(modal.service_type);
      if(modal.service_name.length <= 1){
        toast.error("Vui lòng nhập tên dịch vụ",{position: toast.POSITION.TOP_CENTER});
        return;
      }
      if(isUpdate){
        dichVuService.updateDichVu(modal,user.token).then((res) => {
          toast.success("Cập nhật dịch vụ thành công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
              })
              .catch((err) => {
                toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
              });
      }else{
        dichVuService.createDichVu(modal,user.token).then((res) => {
          toast.success("Tạo dịch vụ thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
        })
        .catch((err) => {
          toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
        });
      }
    }
    let handleDeleteService = (id) => {
      dichVuService.updateDichVu({service_id:id,service_status:0},user.token).then((res) => {
          toast.success("Xoá dịch vụ thành công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
            })
            .catch((err) => {
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }
    let handleUploadAnh = (service_id) => {
      let files = document.getElementById("img_service").files;
      dichVuService.uploadImg(files[0],service_id,user.token).then((res) => {
        toast.success("Upload Hình Thành Công",{position: toast.POSITION.TOP_CENTER});
        window.location.reload();
            })
            .catch((err) => {
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }
    let renderDichVu = () => {
      return dichVuArray.map((dichVu,index) => {
        return(
          <tr>
            <td><p>{index + 1}</p></td>
            <td><p>{dichVu.service_name}</p></td>
            <td><button onClick={() => handleSetModal(index)}  type="button" className='border-0 bg-body text-decoration-underline text-primary' data-bs-toggle="modal" data-bs-target="#modalUpload">Quản Lý</button></td>
            <td><p>{dichVu.service_detail}</p></td>
            <td><p>{new Intl.NumberFormat('en-US').format(dichVu.service_price)}</p></td>
            <td><p>{dichVu.danhmuc?.danhmuc_ten}</p></td>
            <td><p>{renderLoaiDichVu(dichVu.service_type)}</p></td>
            <td>{renderStatus(dichVu.service_status)}</td>
            <td>{renderEditButton(index)}{renderDeleteButton(dichVu.service_id)}</td>
          </tr>
        )
      })
    }
    let renderDichVuPage = () => {
      if(access_page[3].access_status == 0){
        return(
          <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
        )
      }else{
        return(
          <>
            <table class="table table-bordered">
              <thead>
                <th style={{width:"40px"}}>{renderAddButton()}</th>
                <th>Tên Dịch Vụ</th>
                <th>Hình Ảnh</th>
                <th>Chi Tiết</th>
                <th>Giá Tiền</th>
                <th>Danh Mục</th>
                <th>Loại Dịch Vụ</th>
                <th>Trạng Thái</th>
                <th style={{width:"90px"}}>Thao Tác</th>
              </thead>
              <tbody id='myTable'>
                  {renderDichVu()}
              </tbody>
            </table>
          </>
        )
      }
    }
    let renderModal = () => {
      return(
        <div class="modal fade modal-xl" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div style={{background:"#037ec1"}} class="modal-header">
                <div></div>
                <h1 class="modal-title fs-2 text-light" id="exampleModalLabel">Thêm Dịch Vụ</h1>
                <button style={{marginLeft:"0"}} type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <div className='row'>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Tên Dịch Vụ</label>
                      <div>
                        <input onChange={changeModal} type="text" id="service_name" class="form-control input-lg" name="service_name" defaultValue={modal.service_name} placeholder="Nhập tên dịch vụ"></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Mô Tả Dịch Vụ</label>
                      <div>
                        <input onChange={changeModal} type="text" id="service_detail" class="form-control input-lg" name="service_detail" defaultValue={modal.service_detail} placeholder="Nhập mô tả dịch vụ"></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Giá Tiền</label>
                      <div>
                        <input onChange={changeModal} type="text" id="service_price" class="form-control input-lg" name="service_price" defaultValue={modal.service_price} placeholder="Nhập Giá Dịch Vụ"></input>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='row mt-3'>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Danh Mục</label>
                      <div>
                        <select onChange={changeModal} id="danhmuc_id" class="form-control input-lg" name="danhmuc_id" defaultValue={1}>{renderSelectDanhMuc(modal.danhmuc_id)}</select>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Loại Dịch Vụ</label>
                      <div>
                      <select onChange={changeModal} id="service_type" class="form-control input-lg" name="service_type" defaultValue={0}>{renderSelectLoai(modal.service_type)}</select>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Trạng Thái</label>
                      <div>
                      <select onChange={changeModal} id="service_status" class="form-control input-lg" name="service_status">
                          {renderSelectStatus(modal.service_status)}
                      </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                <button onClick={handleCreateUpdate} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
              </div>
            </div>
          </div>
        </div>
      )
    }
    // modal upload ảnh
    let renderModalUpload = () => {
      return(
        <div class="modal fade" id="modalUpload" tabindex="-1" aria-labelledby="modalUploadLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                
                <h1 class="modal-title fs-5" id="modalUploadLabel">Quản Lý Ảnh</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                  <a target="_blank" href={modal.service_img}><img src={modal.service_img} width={"90%"} alt="" /></a>
                  <div className='upload mt-3'>
                    <h5>Thay Đổi Ảnh</h5>
                    <input className="form-control"
                        type="file" accept='image/*'
                        id='img_service'
                        name = "img_service"
                        />
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                <button onClick={() => handleUploadAnh(modal.service_id)} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
              </div>
            </div>
          </div>
        </div>
      )
    }
  return (
    <div id='dichVu'>
        <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Danh Sách Dịch Vụ</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input autoComplete='off' placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
          </div>
          {renderDichVuPage()}
          {renderModal()}
          {renderModalUpload()}
        </div>
        <ToastContainer></ToastContainer>
    </div>
  )
}
