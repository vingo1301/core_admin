import React, { useEffect, useState } from 'react'
import { sessionService } from '../../services/sessionService/SessionService';
import { danhGiaService } from '../../services/danhGiaService/danhGiaService';
import moment from 'moment/moment';

export default function DanhGia() {
    let user = sessionService.getUser();
    let access_page = JSON.parse(user.access_page);
    let action_page = JSON.parse(user.action_page);
    let [danhGiaList,setDanhGiaList] = useState([]);
    let [danhGiaListClone,setDanhGiaListClone] = useState([]);
    useEffect(() => {
        danhGiaService.getDanhGia(user.token).then((res) => {
                setDanhGiaList(res.data);
                setDanhGiaListClone(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
      },[])
    let sortArray = (e) => {
        let array = danhGiaListClone.slice()
        if(e.target.value == 0){
          array.sort(function(a,b){
            var nameA = a.danhgia_createdate;
            var nameB = b.danhgia_createdate;
            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
    
            // name trùng nhau
            return 0;
          });
        }else{
          array.sort(function(a,b){
            var nameA = a.danhgia_createdate;
            var nameB = b.danhgia_createdate;
            if (nameA > nameB) {
              return -1;
            }
            if (nameA < nameB) {
              return 1;
            }
    
            // name trùng nhau
            return 0;
          });
        }
        setDanhGiaList(array);
    }
    let renderDanhGiaItem = () => {
        return danhGiaList.map((danhGia,index) => {
            return <tr>
                <td>{index+1}</td>
                <td>{danhGia?.order?.services?.service_name}</td>
                <td>{danhGia?.order?.order_code}</td>
                <td>{danhGia?.user_app_khach?.user_username}:{danhGia?.user_app_khach?.user_fullname}</td>
                <td>{danhGia?.order?.user_app_tho?.user_username}:{danhGia?.order?.user_app_tho?.user_fullname}</td>
                <td>{danhGia?.danhgia_rating}</td>
                <td>{danhGia?.danhgia_comment}</td>
                <td>{danhGia?.danhgia_createdate?moment(danhGia.danhgia_createdate).format("DD/MM/YYYY"):""}</td>
            </tr>
        })
    }
    let renderDanhGia = () => {
        if(access_page[12].access_status == 0){
            return(
              <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
            )
          }else{
            return(
              <>
                <table class="table table-bordered">
                  <thead>
                    <th style={{width:"40px"}}></th>
                    <th>Tên Dịch Vụ</th>
                    <th>Mã Đơn</th>
                    <th>Người Đánh Giá</th>
                    <th>Thợ Nhận</th>
                    <th>Sao Đánh Giá</th>
                    <th>Nội Dung</th>
                    <th>Thời Gian</th>
                  </thead>
                  <tbody id='myTable'>
                      {renderDanhGiaItem()}
                  </tbody>
                </table>
              </>
            )
        }
    }
  return (
    <div id='danhGia'>
      <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Danh Sách Đánh Giá</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input autoComplete='off' placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
            <select onChange={sortArray} style={{marginLeft:"20px",borderRadius:"5px",borderColor:"#d5d5d5"}} name="" id="">
              <option selected value="0">Ngày Đánh Giá Xa Nhất</option>
              <option value="1">Ngày Đánh Giá Gần Nhất</option>
            </select>
          </div>
          {renderDanhGia()}
        </div>
    </div>
  )
}
