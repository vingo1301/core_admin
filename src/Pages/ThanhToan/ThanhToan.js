import React, { useEffect, useState } from 'react'
import { thanhToanService } from '../../services/thanhToan/thanhToanService'
import { sessionService } from '../../services/sessionService/SessionService';
import moment from 'moment';

export default function ThanhToan() {
  let user = sessionService.getUser();
  let access_page = JSON.parse(user.access_page);
  let currentDay = moment().format("YYYY-MM-DD");
  useEffect(() => {
    thanhToanService.getDonHang(user.token,currentDay).then((res) => {
            setOrderArray(res.data);
          })
          .catch((err) => {
           console.log(err);
          });
  },[]);
  let [orderArray,setOrderArray] = useState([]);
  let renderThanhToanItem = () => {
    return orderArray.map((order,index) => {
      return <tr>
        <td>{index+1}</td>
        <td>{order.order_payment_gate}</td>
        <td>{new Intl.NumberFormat('en-US').format(order._sum.order_price)}</td>
      </tr>
    })
  }
  let renderTotal = () => {
    let total = 0;
    orderArray.map((order) => {
      total += order._sum.order_price;
    })
    return new Intl.NumberFormat('en-US').format(total) ;
  }
  let changeDay = (e) => {
    thanhToanService.getDonHang(user.token,e.target.value).then((res) => {
            setOrderArray(res.data)
          })
          .catch((err) => {
           console.log(err);
          });
  }
  let renderThanhToan = () => {
    if(access_page[8].access_status == 0){
      return(
        <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
      )
    }else{
      return(
        <>
          <table class="table table-bordered">
            <thead>
              <th style={{width:"40px"}}>{}</th>
              <th>Phương Thức Thanh Toán</th>
              <th>Số Tiền</th>
            </thead>
            <tbody id='myTable'>
                {renderThanhToanItem()}
                <tr>
                  <td></td>
                  <td>Tổng</td>
                  <td>{renderTotal()}</td>
                </tr>
            </tbody>
          </table>
        </>
      )
  }
  }
  return (
    <div id='thanhToan'>
      <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Thống Kê Doanh Thu</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
          </div>
          <div className='text-start mb-3'>
            <span>Chọn Ngày: </span>
            <input onChange={changeDay} defaultValue={currentDay} type="date" />
          </div>
          {renderThanhToan()}
        </div>
    </div>
  )
}
