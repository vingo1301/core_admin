import React, { useEffect, useState } from 'react';
import { thanhToanService } from '../../services/thanhToan/thanhToanService'
import { sessionService } from '../../services/sessionService/SessionService';
import moment from 'moment';
import { Popconfirm } from 'antd';
import icon_add from "../../isset/img/icon/ICON ADD.png"
import icon_edit from "../../isset/img/icon/Báo cáo tổng-01.png"
import icon_delete from "../../isset/img/icon/ICON REMOVE.png"
import { ToastContainer, toast } from 'react-toastify';

export default function ThanhToanNew() {
    let user = sessionService.getUser();
    let access_page = JSON.parse(user.access_page);
    let action_page = JSON.parse(user.action_page);
    let [modal,setModal] = useState({});
    let [isUpdate,setIsUpdate] = useState(false);
    let [thanhToanArray,setThanhToanArray] = useState([]);

    useEffect(() => {
        thanhToanService.getThanhToan(user.token).then((res) => {
                setThanhToanArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
    },[]);
    let handleSetModal = (id) => {
      if(id == -1){
        let tmpModal = {
          thanhtoan_name:"",
          thanhtoan_code:"",
          thanhtoan_status: "0",
          thanhtoan_chuthe:"",
          thanhtoan_nganghang:"",
          thanhtoan_stk:""
        }
        setModal(tmpModal);
        setIsUpdate(false);
      }else{
        setModal(thanhToanArray[id]);
        setIsUpdate(true);
      }
    }
    let changeModal = (e) => {
      modal[e.target.name] = e.target.value;
    }
    let handleCreateUpdate = () => {
      modal.thanhtoan_status = Number(modal.thanhtoan_status);
      let file = document.getElementById("thanhtoan_img").files[0];
      if(modal.thanhtoan_name.length <= 1){
        toast.error("Vui lòng nhập tên phương thức thanh toán",{position: toast.POSITION.TOP_CENTER});
        return;
      }
      if(modal.thanhtoan_code.length <= 1){
        toast.error("Vui lòng nhập mã phương thức thanh toán",{position: toast.POSITION.TOP_CENTER});
        return;
      }
      if(isUpdate){
        thanhToanService.updateThanhToan(user.token,modal,file).then((res) => {
          toast.success("Cập nhật thanh toán thành công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
              })
              .catch((err) => {
                toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
              });
      }else{
        thanhToanService.createThanhToan(user.token,modal,file).then((res) => {
          toast.success("Tạo thanh toán thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
        })
        .catch((err) => {
          console.log(err);
          toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
        });
      }
    }
    let handleDeleteService = (id) => {
    thanhToanService.updateThanhToan(user.token,{thanhtoan_id:id,thanhtoan_status:0}).then((res) => {
        toast.success("Xoá thanh toán thành công",{position: toast.POSITION.TOP_CENTER});
        window.location.reload();
          })
          .catch((err) => {
            toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
          });
    }
    let renderSelectStatus = (status) => {
      let status_array = ["Đóng","Mở"];
      return status_array.map((item,index) => {
        if(index == status){
          return <option selected value={index}>{status_array[index]}</option>
        }else{
          return <option value={index}>{status_array[index]}</option>
        }
      })
    }
    let renderAddButton = () => {
      if(action_page[8]?.action_create == 1){
        return <button onClick={() => handleSetModal(-1)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0"}}>
          <img width={"20px"} height={"20px"} src={icon_add} alt="" />
        </button>
      }
    }
    let renderEditButton = (id) => {
      if(action_page[8].action_update == 1){
        return <button onClick={() => handleSetModal(id)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0",background: "transparent",marginRight:"7px"}}>
          <img width={"20px"} height={"20px"} src={icon_edit} alt="" />
        </button>
      }
    }
    let renderDeleteButton = (id) => {
      if(action_page[8].action_delete == 1){
        return <Popconfirm
        title="Xoá Thanh Toán"
        description="Bạn Chắc chắn xoá Thanh Toán này?"
        okText="Xoá"
        cancelText="Huỷ"
        onConfirm={() => {handleDeleteService(id)}}
      >
        <button style={{border:"none",padding:"0",background: "transparent"}}>
          <img width={"20px"} height={"20px"} src={icon_delete} alt="" />
        </button>
      </Popconfirm>
      }
  }
    let renderThanhToanItem = () => {
        return thanhToanArray.map((thanhToan,index) => {
          return <tr>
            <td>{index+1}</td>
            <td>{thanhToan.thanhtoan_name}</td>
            <td>{thanhToan.thanhtoan_code}</td>
            <td>{thanhToan?.thanhtoan_nganhang}</td>
            <td>{thanhToan?.thanhtoan_chuthe}</td>
            <td>{thanhToan?.thanhtoan_stk}</td>
            <td><a href={thanhToan.thanhtoan_img} target="_blank" rel="noopener noreferrer">Xem</a></td>
            <td>{thanhToan.thanhtoan_status==1?<p className='text-success'>Hoạt Động</p>:<p className='text-danger'>Ngưng</p>}</td>
            <td>{renderEditButton(index)}{renderDeleteButton(thanhToan.thanhtoan_id)}</td>
          </tr>
        })
      }
    let renderThanhToan = () => {
        if(access_page[8].access_status == 0){
          return(
            <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
          )
        }else{
          return(
            <>
              <table class="table table-bordered">
                <thead>
                  <th style={{width:"40px"}}>{renderAddButton()}</th>
                  <th>Phương Thức Thanh Toán</th>
                  <th>Mã Phương Thức</th>
                  <th>Ngân Hàng</th>
                  <th>Tên Chủ Thẻ</th>
                  <th>Số Tài Khoản</th>
                  <th>Hình Ảnh</th>
                  <th>Trạng Thái</th>
                  <th>Thao Tác</th>
                </thead>
                <tbody id='myTable'>
                    {renderThanhToanItem()}
                </tbody>
              </table>
            </>
          )
      }
    }
    let renderModal = () => {
        return(
          <div class="modal fade modal-xl" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div style={{background:"#037ec1"}} class="modal-header">
                  <div></div>
                  <h1 class="modal-title fs-2 text-light" id="exampleModalLabel">Thêm Thanh Toán</h1>
                  <button style={{marginLeft:"0"}} type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <div className='row'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Tên Phương Thức Thanh Toán</label>
                        <div>
                          <input onChange={changeModal} type="text" id="thanhtoan_name" class="form-control input-lg" name="thanhtoan_name" defaultValue={modal.thanhtoan_name} placeholder="Nhập tên thanh toán"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Mã Phương Thức (Không dấu, viết liền)</label>
                        <div>
                          <input onChange={changeModal} type="text" id="thanhtoan_code" class="form-control input-lg" name="thanhtoan_code" defaultValue={modal.thanhtoan_code} placeholder="Nhập mã thanh toán"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                        <div className='form-group'>
                            <label class="control-label">Trạng Thái</label>
                            <div>
                                <select onChange={changeModal} id="thanhtoan_status" class="form-control input-lg" name="thanhtoan_status">
                                    {renderSelectStatus(modal.thanhtoan_status)}
                                </select>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div className='row mt-3'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Hình Ảnh Thanh Toán</label>
                        <div>
                          <input type="file" accept='image/*' id="thanhtoan_img" class="form-control input-lg" name="thanhtoan_img"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                        <div className='form-group'>
                            <label class="control-label">Tên Ngân Hàng</label>
                            <div>
                              <input onChange={changeModal} type="text" id="thanhtoan_nganhang" class="form-control input-lg" name="thanhtoan_nganhang" defaultValue={modal.thanhtoan_nganhang} placeholder="Nhập Tên Ngân Hàng"></input>
                            </div>
                        </div>
                    </div>
                    <div className='col-4'>
                        <div className='form-group'>
                            <label class="control-label">Tên Chủ Thẻ</label>
                            <div>
                              <input onChange={changeModal} type="text" id="thanhtoan_chuthe" class="form-control input-lg" name="thanhtoan_chuthe" defaultValue={modal.thanhtoan_chuthe} placeholder="Nhập Tên Chủ Thẻ"></input>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div className='row mt-3'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Số Tài Khoản</label>
                        <div>
                          <input onChange={changeModal} type="text" id="thanhtoan_stk" class="form-control input-lg" name="thanhtoan_stk" defaultValue={modal.thanhtoan_stk} placeholder="Nhập Số Tài Khoản"></input>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                  <button onClick={handleCreateUpdate} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
                </div>
              </div>
            </div>
          </div>
        )
    }
  return (
    <div id='thanhToan'>
      <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Hệ Thống Thanh Toán</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
          </div>
          {renderThanhToan()}
          {renderModal()}
        </div>
        <ToastContainer></ToastContainer>
    </div>
  )
}
