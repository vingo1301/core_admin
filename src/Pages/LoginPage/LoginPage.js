import React, { useState } from 'react'
import './LoginPage.css'
import { Input } from 'antd'
import { userService } from '../../services/userService/UserService';
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { sessionService } from '../../services/sessionService/SessionService';
import { useNavigate } from 'react-router';
export default function LoginPage() {
    let [username,setUsername] = useState("");
    let [password,setPassword] = useState("");
    let navigate = useNavigate();
    let user = sessionService.getUser();
    if(user != null){
        setTimeout(() => {
            navigate("/app-khach")
        },0)
    }
    let changeUsername = (e) => {
        setUsername(e.target.value);
    }
    let changePassword = (e) => {
        setPassword(e.target.value);
    }
    let handleLogin = () => {
        if(username == ""){
            document.getElementById("err_username").innerHTML = "Vui Lòng Nhập Tài Khoản!";
        }else{
            document.getElementById("err_username").innerHTML = "";
        }
        if(password == ""){
            document.getElementById("err_password").innerHTML = "Vui Lòng Nhập Mật Khẩu!";
        }else{
            document.getElementById("err_password").innerHTML = "";
        }
        userService.getLogin({username,password}).then((res) => {
                sessionService.setUser(res.data);
                toast.success("Đăng Nhập thành công!",{position: toast.POSITION.TOP_CENTER})
                setTimeout(() => {
                    navigate("/")
                },1000)
              })
              .catch((err) => {
                // console.log(err);
               toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER})
              });
    }
  return (
    <>
    <div id='login'>
        <div className='loginForm rounded-lg' onKeyDown={function (e) {
        e.key == "Enter" && handleLogin();
      }}>
           <h2 className='fs-3 fw-bolder mt-3'>ĐĂNG NHẬP</h2>
           <div className='p-4'>
                <div className='mb-3'>
                    <Input placeholder='Tài Khoản' onChange={changeUsername} />
                    <p className='text-danger' id='err_username'></p>
                </div>
                <div>
                    <Input.Password placeholder='Mật Khẩu' onChange={changePassword} />
                    <p className='text-danger' id='err_password'></p>
                </div>
                <div>
                    <button className='mt-3' onClick={handleLogin}>Đăng nhập</button>
                </div>
                
           </div>
        </div>
    </div>
    <ToastContainer></ToastContainer>
    </>
    
  )
}
