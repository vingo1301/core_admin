import React, { useEffect, useState } from 'react';
import { sessionService } from '../../services/sessionService/SessionService';
import moment from 'moment';
import { Popconfirm } from 'antd';
import icon_add from "../../isset/img/icon/ICON ADD.png"
import icon_edit from "../../isset/img/icon/Báo cáo tổng-01.png"
import icon_delete from "../../isset/img/icon/ICON REMOVE.png"
import { ToastContainer, toast } from 'react-toastify';
import { danhMucService } from '../../services/danhMuc/danhMucService';

export default function DanhMuc() {
    let user = sessionService.getUser();
    let access_page = JSON.parse(user.access_page);
    let action_page = JSON.parse(user.action_page);
    let [modal,setModal] = useState({});
    let [isUpdate,setIsUpdate] = useState(false);
    let [danhMucArray,setDanhMucArray] = useState([]);
    useEffect(() => {
      danhMucService.getDanhMuc(user.token).then((res) => {
              setDanhMucArray(res.data);
            })
            .catch((err) => {
             console.log(err);
            });
    },[])

    let handleSetModal = (id) => {
        if(id == -1){
          let tmpModal = {
            danhmuc_ten:"",
            danhmuc_status: "0"
          }
          setModal(tmpModal);
          setIsUpdate(false);
        }else{
          setModal(danhMucArray[id]);
          setIsUpdate(true);
        }
    }
    let handleCreateUpdate = () => {
      modal.danhmuc_status = Number(modal.danhmuc_status);
      let file = document.getElementById("danhmuc_hinhanh").files[0];
      if(modal.danhmuc_ten.length <= 1){
        toast.error("Vui lòng nhập tên danh mục",{position: toast.POSITION.TOP_CENTER});
        return;
      }
      if(isUpdate){
        danhMucService.updateDanhMuc(user.token,modal,file).then((res) => {
          toast.success("Cập nhật danh mục thành công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
              })
              .catch((err) => {
                toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
              });
      }else{
        danhMucService.createDanhMuc(user.token,modal,file).then((res) => {
          toast.success("Tạo danh mục thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
        })
        .catch((err) => {
          console.log(err);
          toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
        });
      }
    }
    let handleDeleteService = (id) => {
      danhMucService.updateDanhMuc(user.token,{danhmuc_id:id,danhmuc_status:0}).then((res) => {
          toast.success("Xoá danh mục thành công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
            })
            .catch((err) => {
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
      }
    let changeModal = (e) => {
        modal[e.target.name] = e.target.value;
    }
    let renderSelectStatus = (status) => {
        let status_array = ["Đóng","Mở"];
        return status_array.map((item,index) => {
          if(index == status){
            return <option selected value={index}>{status_array[index]}</option>
          }else{
            return <option value={index}>{status_array[index]}</option>
          }
        })
    }
    let renderAddButton = () => {
        if(action_page[9]?.action_create == 1){
          return <button onClick={() => handleSetModal(-1)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0"}}>
            <img width={"20px"} height={"20px"} src={icon_add} alt="" />
          </button>
        }
      }
    let renderEditButton = (id) => {
        if(action_page[9].action_update == 1){
          return <button onClick={() => handleSetModal(id)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0",background: "transparent",marginRight:"7px"}}>
            <img width={"20px"} height={"20px"} src={icon_edit} alt="" />
          </button>
        }
      }
      let renderDeleteButton = (id) => {
        if(action_page[9].action_delete == 1){
          return <Popconfirm
          title="Xoá Thanh Toán"
          description="Bạn Chắc chắn xoá Thanh Toán này?"
          okText="Xoá"
          cancelText="Huỷ"
          onConfirm={() => {handleDeleteService(id)}}
        >
          <button style={{border:"none",padding:"0",background: "transparent"}}>
            <img width={"20px"} height={"20px"} src={icon_delete} alt="" />
          </button>
        </Popconfirm>
        }
    }
    let renderDanhMucItem = () => {
        return danhMucArray.map((danhMuc,index) => {
          return <tr>
            <td>{index+1}</td>
            <td>{danhMuc.danhmuc_ten}</td>
            <td><a href={danhMuc.danhmuc_hinhanh} target="_blank" rel="noopener noreferrer">Xem</a></td>
            <td>{danhMuc.danhmuc_status==1?<p className='text-success'>Hoạt Động</p>:<p className='text-danger'>Ngưng</p>}</td>
            <td>{renderEditButton(index)}{renderDeleteButton(danhMuc.danhmuc_id)}</td>
          </tr>
        })
    }
    let renderDanhMuc = () => {
        if(access_page[9].access_status == 0){
          return(
            <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
          )
        }else{
          return(
            <>
              <table class="table table-bordered">
                <thead>
                  <th style={{width:"40px"}}>{renderAddButton(-1)}</th>
                  <th>Tên Danh Mục</th>
                  <th>Hình Ảnh</th>
                  <th>Trạng Thái</th>
                  <th>Thao Tác</th>
                </thead>
                <tbody id='myTable'>
                    {renderDanhMucItem()}
                </tbody>
              </table>
            </>
          )
      }
    }
    let renderModal = () => {
        return(
          <div class="modal fade modal-xl" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div style={{background:"#037ec1"}} class="modal-header">
                  <div></div>
                  <h1 class="modal-title fs-2 text-light" id="exampleModalLabel">Thêm Danh Mục</h1>
                  <button style={{marginLeft:"0"}} type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <div className='row'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Tên Danh Mục</label>
                        <div>
                          <input onChange={changeModal} type="text" id="danhmuc_ten" class="form-control input-lg" name="danhmuc_ten" defaultValue={modal.danhmuc_ten} placeholder="Nhập tên danh mục"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Hình Ảnh Danh Mục</label>
                        <div>
                          <input type="file" accept='image/*' id="danhmuc_hinhanh" class="form-control input-lg" name="danhmuc_hinhanh"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                        <div className='form-group'>
                            <label class="control-label">Trạng Thái</label>
                            <div>
                                <select onChange={changeModal} id="danhmuc_status" class="form-control input-lg" name="danhmuc_status">
                                    {renderSelectStatus(modal.danhmuc_status)}
                                </select>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                  <button onClick={handleCreateUpdate} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
                </div>
              </div>
            </div>
          </div>
        )
    }
  return (
    <div id='danhMuc'>
      <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Danh Sách Danh Mục</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
          </div>
          {renderDanhMuc()}
          {renderModal()}
        </div>
        <ToastContainer></ToastContainer>
    </div>
  )
}
