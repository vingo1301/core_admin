import React, { useEffect, useState } from 'react'
import { quangCaoService } from '../../services/quangCaoService/quangCaoService'
import { sessionService } from '../../services/sessionService/SessionService';
import icon_add from "../../isset/img/icon/ICON ADD.png"
import icon_edit from "../../isset/img/icon/Báo cáo tổng-01.png"
import icon_delete from "../../isset/img/icon/ICON REMOVE.png"
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { Popconfirm } from 'antd';

export default function QuangCao() {
    let user = sessionService.getUser();
    let access_page = JSON.parse(user.access_page);
    let action_page = JSON.parse(user.action_page);
    useEffect(() => {
        quangCaoService.getQuangCao(user.token).then((res) => {
                setQuangCaoArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
        quangCaoService.getBanner(user.token).then((res) => {
                setBanner(res.data[0]);
              })
              .catch((err) => {
               console.log(err);
              });
    },[])
    let [quangCaoArray,setQuangCaoArray] = useState([]);
    let [banner,setBanner] = useState({});
    let [modal,setModal] = useState({});
    let [isUpdate,setIsUpdate] = useState(false);

    let handleSetModal = (id) => {
        if(id == -1){
          let tmpModal = {
            ads_name:"",
            ads_desc:"",
            ads_status: "0",
            ads_link:""
          }
          setModal(tmpModal);
          setIsUpdate(false);
        }else{
          setModal(quangCaoArray[id]);
          setIsUpdate(true);
        }
    }
    let changeModal = (e) => {
        modal[e.target.name] = e.target.value;
    }
    let handleCreateUpdate = () => {
        modal.ads_status = Number(modal.ads_status);
        if(modal.ads_name.length <= 1){
          toast.error("Vui lòng nhập tên quảng cáo",{position: toast.POSITION.TOP_CENTER});
          return;
        }
        if(isUpdate){
          quangCaoService.updateQuangCao(modal,user.token).then((res) => {
            toast.success("Cập nhật dịch vụ thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
                })
                .catch((err) => {
                  toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
                });
        }else{
          quangCaoService.createQuangCao(modal,user.token).then((res) => {
            toast.success("Tạo dịch vụ thành công",{position: toast.POSITION.TOP_CENTER});
              window.location.reload();
          })
          .catch((err) => {
            toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
          });
        }
    }
    let handleDeleteService = (id) => {
        quangCaoService.updateQuangCao({ads_id:id,ads_status:0},user.token).then((res) => {
            toast.success("Xoá quảng cáo thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
              })
              .catch((err) => {
                toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
              });
    }
    let handleUploadAnh = (ads_id) => {
        let files = document.getElementById("img_ads").files;
        quangCaoService.uploadImg(files[0],ads_id,user.token).then((res) => {
          toast.success("Upload Hình Thành Công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
              })
              .catch((err) => {
                toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
              });
    }
    let changeUpdateBanner = (e) => {
      let file = e.target.files[0];
      quangCaoService.updateBanner(file,user.token).then((res) => {
          toast.success("Upload Hình Thành Công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
            })
            .catch((err) => {
             console.log(err);
            });
    }
    let changeStatusBanner = (e) => {
      quangCaoService.editStatusBanner({status: e.target.value},user.token).then((res) => {
          toast.success("Cập Nhật Thành Công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
            })
            .catch((err) => {
             console.log(err);
            });
    } 
    let renderStatus = (status) => {
        if(status == 1){
            return <p>Mở</p>
          }else{
            return <p className='text-danger'>Đóng</p>
          }
    }
    let renderSelectStatus = (status) => {
        let status_array = ["Đóng","Mở"];
        return status_array.map((item,index) => {
          if(index == status){
            return <option selected value={index}>{status_array[index]}</option>
          }else{
            return <option value={index}>{status_array[index]}</option>
          }
      })
    }
    let renderStatusBanner = (status) => {
        let status_array = ["Ẩn","Hiển thị"];
        return status_array.map((item,index) => {
          if(index == status){
            return <option selected value={index}>{status_array[index]}</option>
          }else{
            return <option value={index}>{status_array[index]}</option>
          }
        })
      }
    let renderAddButton = () => {
        if(action_page[4]?.action_create == 1){
          return <button onClick={() => handleSetModal(-1)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0"}}>
            <img width={"20px"} height={"20px"} src={icon_add} alt="" />
          </button>
        }
    }
    let renderEditButton = (id) => {
        if(action_page[4].action_update == 1){
          return <button onClick={() => handleSetModal(id)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0",background: "transparent",marginRight:"7px"}}>
            <img width={"20px"} height={"20px"} src={icon_edit} alt="" />
          </button>
        }
    }
      let renderDeleteButton = (id) => {
        if(action_page[4].action_delete == 1){
          return <Popconfirm
          title="Xoá Quảng Cáo"
          description="Bạn Chắc chắn xoá Quảng Cáo này?"
          okText="Xoá"
          cancelText="Huỷ"
          onConfirm={() => {handleDeleteService(id)}}
        >
          <button style={{border:"none",padding:"0",background: "transparent"}}>
            <img width={"20px"} height={"20px"} src={icon_delete} alt="" />
          </button>
        </Popconfirm>
        }
    }
    let renderQuangCaoItem = () => {
        return quangCaoArray.map((quangCao,index) => {
            return(
              <tr>
                <td><p>{index + 1}</p></td>
                <td><p>{quangCao.ads_name}</p></td>
                <td><button onClick={() => handleSetModal(index)} type="button" className='border-0 bg-body text-decoration-underline text-primary' data-bs-toggle="modal" data-bs-target="#modalUpload">Quản Lý</button></td>
                <td><p>{quangCao.ads_desc}</p></td>
                <td><a href={quangCao?.ads_link} target="_blank" rel="noopener noreferrer">{quangCao.ads_link?quangCao.ads_link.slice(0,20)+ "...":""}</a></td>
                <td>{renderStatus(quangCao.ads_status)}</td>
                <td>{renderEditButton(index)}{renderDeleteButton(quangCao.ads_id)}</td>
              </tr>
            )
        })
    }
    let renderBanner = () => {
      return(
        <tr>
          <td>1</td>
          <td><a href={banner?.hinh_anh} target="_blank" rel="noopener noreferrer">Xem</a></td>
          <td><input onChange={changeUpdateBanner} type="file" name="hinh_anh" accept='image/*' /></td>
          <td>
            <select onChange={changeStatusBanner} style={{width:"75%"}} id="status">
              {renderStatusBanner(banner?.status)}
            </select>
          </td>
        </tr>
      )
    }
    let renderQuangCao = () => {
        if(access_page[4].access_status == 0){
            return(
              <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
            )
          }else{
            return(
              <>
                <div>
                  <h2>Banner</h2>
                  <table class="table table-bordered">
                    <thead>
                      <th></th>
                      <th>Hình Ảnh</th>
                      <th>Đổi Ảnh khác</th>
                      <th>Trạng Thái</th>
                    </thead>
                    <tbody >
                        {renderBanner()}
                    </tbody>
                </table>
                </div>
                <h2>Quảng Cáo</h2>
                <table class="table table-bordered">
                  <thead>
                    <th style={{width:"40px"}}>{renderAddButton()}</th>
                    <th>Tên Quảng Cáo</th>
                    <th>Hình Ảnh</th>
                    <th>Chi Tiết</th>
                    <th>Liên Kết</th>
                    <th>Trạng Thái</th>
                    <th style={{width:"90px"}}>Thao Tác</th>
                  </thead>
                  <tbody id='myTable'>
                      {renderQuangCaoItem()}
                  </tbody>
                </table>
              </>
            )
        }
    }
    let renderModal = () => {
        return(
          <div class="modal fade modal-xl" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div style={{background:"#037ec1"}} class="modal-header">
                  <div></div>
                  <h1 class="modal-title fs-2 text-light" id="exampleModalLabel">Thêm Quảng Cáo</h1>
                  <button style={{marginLeft:"0"}} type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <div className='row'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Tên Quảng Cáo</label>
                        <div>
                          <input onChange={changeModal} type="text" id="ads_name" class="form-control input-lg" name="ads_name" defaultValue={modal.ads_name} placeholder="Nhập tên quảng cáo"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Mô Tả Quảng Cáo</label>
                        <div>
                          <input onChange={changeModal} type="text" id="ads_desc" class="form-control input-lg" name="ads_desc" defaultValue={modal.ads_desc} placeholder="Nhập mô tả quảng cáo"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                        <div className='form-group'>
                            <label class="control-label">Trạng Thái</label>
                            <div>
                                <select onChange={changeModal} id="ads_status" class="form-control input-lg" name="ads_status">
                                    {renderSelectStatus(modal.ads_status)}
                                </select>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Liên Kết</label>
                        <div>
                          <input onChange={changeModal} type="text" id="ads_link" class="form-control input-lg" name="ads_link" defaultValue={modal.ads_link} placeholder="Nhập liên kết"></input>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                  <button onClick={handleCreateUpdate} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
                </div>
              </div>
            </div>
          </div>
        )
    }
    // modal upload ảnh
    let renderModalUpload = () => {
        return(
          <div class="modal fade" id="modalUpload" tabindex="-1" aria-labelledby="modalUploadLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  
                  <h1 class="modal-title fs-5" id="modalUploadLabel">Quản Lý Ảnh</h1>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <a target="_blank" href={modal.ads_img}><img src={modal.ads_img} width={"90%"} alt="" /></a>
                    <div className='upload mt-3'>
                      <h5>Thay Đổi Ảnh</h5>
                      <input className="form-control"
                          type="file" accept='image/*'
                          id='img_ads'
                          name = "img_ads"
                          />
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                  <button onClick={() => handleUploadAnh(modal.ads_id)} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
                </div>
              </div>
            </div>
          </div>
        )
      }
  return (
    <div id='quangCao'>
        <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Danh Sách Quảng Cáo</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input autoComplete='off' placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
          </div>
            {renderQuangCao()}
            {renderModal()}
            {renderModalUpload()}
        </div>
        <ToastContainer></ToastContainer>
    </div>
  )
}
