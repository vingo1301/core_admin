import React, { useEffect, useState } from 'react'
import { baoHanhService } from '../../services/baoHanhService/baoHanhService';
import { sessionService } from '../../services/sessionService/SessionService';
import moment from 'moment/moment';
import icon_edit from "../../isset/img/icon/Báo cáo tổng-01.png"
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

export default function BaoHanh() {
    let user = sessionService.getUser();
    let access_page = JSON.parse(user.access_page);
    let action_page = JSON.parse(user.action_page);
    let now = new Date();
    let [day1,setDay1] = useState(String(now.getFullYear() + 1) + "-" + String(now.getMonth()+1<10?"0" + (now.getMonth()+1):now.getMonth()+1) + "-" + "01");
    let [day2,setDay2] = useState(String(now.getFullYear() + 1) + "-" + String(now.getMonth()+1<10?"0"+ (now.getMonth()+1):now.getMonth()+1) + "-" + "31");
    let [baoHanhArray,setBaoHanhArray] = useState([]);
    let [modal,setModal] = useState({});
    let [isUpdate,setIsUpdate] = useState(false);
    useEffect(() => {
        baoHanhService.getBaoHanh(user.token,day1,day2).then((res) => {
                setBaoHanhArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
      },[])
      let handleCreateUpdate = () => {
        modal.baohanh_status = Number(modal.baohanh_status);
        if(isUpdate){
          baoHanhService.updateBaoHanh(modal,user.token).then((res) => {
            toast.success("Cập nhật bảo hành thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
                })
                .catch((err) => {
                  toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
                });
        }
      }
      let changeModal = (e) => {
        modal[e.target.name] = e.target.value;
      }
      let handleSearch = () => {
            baoHanhService.getBaoHanh(user.token,day1,day2).then((res) => {
                setBaoHanhArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
      }
      let handleSetModal = (id) => {
        if(id == -1){
          let tmpModal = {
            baohanh_date: "",
            baohanh_lydo:"",
            baohanh_status: "0"
          }
          setModal(tmpModal);
          setIsUpdate(false);
        }else{
          setModal(baoHanhArray[id]);
          setIsUpdate(true);
        }
      }
      let setDaySelect = (e) => {
        if(e.target.name == "day1" ){
            setDay1(e.target.value);
        };
        if(e.target.name == "day2"){
            setDay2(e.target.value);
        }
      }
      let renderSelectStatus = (status) => {
        let status_array = ["Ngưng Bảo Hành","Vẫn Bảo Hành"];
        return status_array.map((item,index) => {
          if(index == status){
            return <option selected value={index}>{status_array[index]}</option>
          }else{
            return <option value={index}>{status_array[index]}</option>
          }
        })
      }
      let renderSelect = () => {
        return <div className='text-start mb-3'>
            <span className='me-2'>Từ</span>
            <input onChange={setDaySelect} defaultValue={day1} type="date" name='day1' />
            <span className='m-2'>Đến</span>
            <input onChange={setDaySelect} defaultValue={day2} type="date" name='day2' />
            <button onClick={handleSearch} style={{background:"#3498db"}} className='ms-2 border-0 rounded-2 px-2 py-1 text-light'>Lọc</button>
        </div>
      }
      let renderEditButton = (id) => {
        if(action_page[11].action_update == 1){
          return <button onClick={() => handleSetModal(id)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0",background: "transparent",marginRight:"7px"}}>
            <img width={"20px"} height={"20px"} src={icon_edit} alt="" />
          </button>
        }
      }
      let renderBaoHanhItem = () => {
        return baoHanhArray.map((baoHanh,index) => {
          return <tr>
            <td>{index+1}</td>
            <td>{baoHanh?.orders?.order_code}</td>
            <td>{baoHanh?.orders?.services?.service_name}</td>
            <td>{moment(baoHanh?.orders?.order_finish_date).format("HH:mm:ss DD-MM-YYYY")}</td>
            <td>{moment(baoHanh?.baohanh_date).format("HH:mm:ss DD-MM-YYYY")}</td>
            <td>{baoHanh?.orders?.order_address}</td>
            <td>{baoHanh?.orders?.user_app_khach?.user_fullname}</td>
            <td>{baoHanh?.orders?.user_app_khach?.user_phone}</td>
            <td>{baoHanh.baohanh_status==0?<p className='text-danger'>Huỷ Bảo Hành</p>:<p className='text-success'>Được Bảo Hành</p>}</td>
            <td>{baoHanh.baohanh_lydo}</td>
            <td>{renderEditButton(index)}</td>
          </tr>
        })
      }
      let renderBaoHanh = () => {
        if(access_page[11].access_status == 0){
          return(
            <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
          )
        }else{
          return(
            <>
              <table class="table table-bordered">
                <thead>
                  <th style={{width:"40px"}}></th>
                  <th>Mã Đơn Hàng</th>
                  <th>Tên Dịch Vụ</th>
                  <th>Ngày Hoàn Thành</th>
                  <th>Ngày Hết Hạn</th>
                  <th>Địa Chỉ</th>
                  <th>Người Đặt</th>
                  <th>Số Điện Thoại</th>
                  <th>Trạng Thái</th>
                  <th>Lý Do Huỷ Bảo Hành</th>
                  <th>Thao Tác</th>
                </thead>
                <tbody id='myTable'>
                    {renderBaoHanhItem()}
                </tbody>
              </table>
            </>
          )
      }
    }
    let renderModal = () => {
      return(
        <div class="modal fade modal-xl" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div style={{background:"#037ec1"}} class="modal-header">
                <div></div>
                <h1 class="modal-title fs-2 text-light" id="exampleModalLabel">Chỉnh Sửa Bảo Hành</h1>
                <button style={{marginLeft:"0"}} type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <div className='row'>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Thời Gian Bảo Hành</label>
                      <div>
                        <input onChange={changeModal} type="datetime-local" id="baohanh_date" class="form-control input-lg" name="baohanh_date" defaultValue={modal.baohanh_date}></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Lý Do Huỷ Bảo Hành</label>
                      <div>
                        <input onChange={changeModal} type="text" id="baohanh_lydo" class="form-control input-lg" name="baohanh_lydo" defaultValue={modal.baohanh_lydo} placeholder="Nhậplý do huỷ bảo hành"></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                      <div className='form-group'>
                          <label class="control-label">Trạng Thái</label>
                          <div>
                              <select onChange={changeModal} id="baohanh_status" class="form-control input-lg" name="baohanh_status">
                                  {renderSelectStatus(modal.baohanh_status)}
                              </select>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                <button onClick={handleCreateUpdate} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
              </div>
            </div>
          </div>
        </div>
      )
  }
  return (
    <div id='baoHanh'>
      <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Danh Sách Bảo Hành</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
          </div>
          {renderSelect()}
          {renderBaoHanh()}
          {renderModal()}
        </div>
        <ToastContainer></ToastContainer>
    </div>
  )
}
