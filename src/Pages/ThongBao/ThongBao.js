import React, { useEffect, useState } from 'react'
import { thongBaoService } from '../../services/thongBaoService/thongBaoService'
import { sessionService } from '../../services/sessionService/SessionService';
import icon_add from "../../isset/img/icon/ICON ADD.png"
import icon_edit from "../../isset/img/icon/Báo cáo tổng-01.png"
import icon_delete from "../../isset/img/icon/ICON REMOVE.png"
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { userService } from '../../services/userService/UserService';
import { Popconfirm } from 'antd';

export default function ThongBao() {
    let user = sessionService.getUser();
    let access_page = JSON.parse(user.access_page);
    let action_page = JSON.parse(user.action_page);
    useEffect(() => {
        thongBaoService.getThongBao(user.token).then((res) => {
                setThongBaoArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
        userService.getUserTho(user.token).then((res) => {
                setThoArray(res.data);
              })
              .catch((err) => {
               console.log(err);
              });
        userService.getUserKhach(user.token).then((res) => {
                setKhachArray(res.data)
              })
              .catch((err) => {
               console.log(err);
              });
    },[]);
    let [thongBaoArray,setThongBaoArray] = useState([]);
    let [khachArray,setKhachArray] = useState([]);
    let [thoArray,setThoArray] = useState([]);
    let [modal,setModal] = useState({});
    let [isUpdate,setIsUpdate] = useState(false);

    let handleSetModal = (id) => {
        if(id == -1){
          let tmpModal = {
            noti_name:"",
            noti_desc:"",
            noti_status: "0",
            noti_cus_id: -1,
            noti_tho_id: -1
          }
          setModal(tmpModal);
          setIsUpdate(false);
        }else{
          setModal(thongBaoArray[id]);
          setIsUpdate(true);
        }
    }
    let changeModal = (e) => {
        modal[e.target.name] = e.target.value;
    }
    let handleCreateUpdate = () => {
        if(modal.noti_cus_id != null && modal.noti_cus_id !=""){
            modal.noti_cus_id = Number(modal.noti_cus_id);
        }
        if(modal.noti_tho_id != null && modal.noti_tho_id !=""){
            modal.noti_tho_id = Number(modal.noti_tho_id);
        }
        modal.noti_status = Number(modal.noti_status);
        
        if(modal.noti_name.length <= 1){
          toast.error("Vui lòng nhập tên thông báo",{position: toast.POSITION.TOP_CENTER});
          return;
        }
        if(isUpdate){
          thongBaoService.updateThongBao(modal,user.token).then((res) => {
            toast.success("Cập nhật thông báo thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
                })
                .catch((err) => {
                  console.log(err);
                  toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
                });
        }else{
          thongBaoService.createThongBao(modal,user.token).then((res) => {
            toast.success("Tạo thông báo thành công",{position: toast.POSITION.TOP_CENTER});
              window.location.reload();
          })
          .catch((err) => {
            toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
          });
        }
    }
    let handleDeleteNoti = (id) => {
      thongBaoService.updateThongBao({noti_id:id,noti_status:0},user.token).then((res) => {
          toast.success("Xoá thông báo thành công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
            })
            .catch((err) => {
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }
    let renderAddButton = () => {
        if(action_page[5]?.action_create == 1){
          return <button onClick={() => handleSetModal(-1)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0"}}>
            <img width={"20px"} height={"20px"} src={icon_add} alt="" />
          </button>
        }
    }
    let renderEditButton = (id) => {
      if(action_page[5].action_update == 1){
        return <button onClick={() => handleSetModal(id)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0",background: "transparent",marginRight:"7px"}}>
          <img width={"20px"} height={"20px"} src={icon_edit} alt="" />
        </button>
      }
    }
    let renderDeleteButton = (id) => {
      if(action_page[5].action_delete == 1){
        return <Popconfirm
        title="Xoá Tài Khoản"
        description="Bạn Chắc chắn xoá thông báo này?"
        okText="Xoá"
        cancelText="Huỷ"
        onConfirm={() => {handleDeleteNoti(id)}}
      >
        <button style={{border:"none",padding:"0",background: "transparent"}}>
          <img width={"20px"} height={"20px"} src={icon_delete} alt="" />
        </button>
      </Popconfirm>
      }
    }
    let renderStatus = (status) => {
        if(status == 1){
            return <p>Mở</p>
          }else{
            return <p className='text-danger'>Đóng</p>
          }
    }
    let renderSelectStatus = (status) => {
        let status_array = ["Tắt","Mở"];
        return status_array.map((item,index) => {
          if(index == status){
            return <option selected value={index}>{status_array[index]}</option>
          }else{
            return <option value={index}>{status_array[index]}</option>
          }
        })
    }
    let renderTenKhach = (user_id) => {
        if(user_id == -1){
            return <p></p>;
        }
        if(user_id == 0){
            return <p>Tất Cả</p>
        }
        let index = khachArray?.findIndex((khach) => {
            return khach.user_id == user_id;
        })
        return <p>{khachArray[index]?.user_username} : {khachArray[index]?.user_fullname}</p>
    }
    let renderSelectKhach = (user_id) => {
        let cloneKhachUser = [{user_id:-1,user_fullname:"Tắt"},{user_id:0,user_fullname:"Tất Cả"},...khachArray];
        return cloneKhachUser.map((khach) => {
            if(khach.user_id == user_id){
                return <option selected value={khach.user_id}>{khach.user_username} - {khach.user_fullname}</option>
            }else{
                return <option value={khach.user_id}>{khach.user_username} - {khach.user_fullname}</option>
            }
        })
    }
    let renderTenTho = (user_id) => {
        if(user_id == -1){
            return <p></p>;
        }
        if(user_id == 0){
            return <p>Tất Cả</p>
        }
        let index = khachArray?.findIndex((khach) => {
            return khach.user_id == user_id;
        })
        return <p>{thoArray[index]?.user_username} : {thoArray[index]?.user_fullname}</p>
    }
    let renderSelectTho = (user_id) => {
        let cloneThoUser = [{user_id:-1,user_fullname:"Tắt"},{user_id:0,user_fullname:"Tất Cả"},...thoArray];
        return cloneThoUser.map((tho) => {
            if(tho.user_id == user_id){
                return <option selected value={tho.user_id}>{tho.user_username} - {tho.user_fullname}</option>
            }else{
                return <option value={tho.user_id}>{tho.user_username} - {tho.user_fullname}</option>
            }
        })
    }
    let renderThongBaoItem = () => {
        return thongBaoArray.map((thongBao,index) => {
            return(
              <tr>
                <td><p>{index + 1}</p></td>
                <td><p>{thongBao.noti_name}</p></td>
                <td><p>{thongBao.noti_desc}</p></td>
                <td>{renderTenKhach(thongBao.noti_cus_id)}</td>
                <td>{renderTenTho(thongBao.noti_tho_id)}</td>
                <td>{renderStatus(thongBao.noti_status)}</td>
                <td>{renderEditButton(index)}{renderDeleteButton(thongBao.noti_id)}</td>
              </tr>
            )
        })
    }
    let renderThongBao = () => {
        if(access_page[5].access_status == 0){
            return(
              <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
            )
          }else{
            return(
              <>
                <table class="table table-bordered">
                  <thead>
                    <th style={{width:"40px"}}>{renderAddButton()}</th>
                    <th>Tên Thông Báo</th>
                    <th>Mô Tả</th>
                    <th>Khách Nhận</th>
                    <th>Thợ Nhận</th>
                    <th>Trạng Thái</th>
                    <th style={{width:"90px"}}>Thao Tác</th>
                  </thead>
                  <tbody id='myTable'>
                      {renderThongBaoItem()}
                  </tbody>
                </table>
              </>
            )
        }
    }
    let renderModal = () => {
        return(
          <div class="modal fade modal-xl" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div style={{background:"#037ec1"}} class="modal-header">
                  <div></div>
                  <h1 class="modal-title fs-2 text-light" id="exampleModalLabel">Thêm Thông Báo</h1>
                  <button style={{marginLeft:"0"}} type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <div className='row'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Tên Thông Báo</label>
                        <div>
                          <input onChange={changeModal} type="text" id="noti_name" class="form-control input-lg" name="noti_name" defaultValue={modal.noti_name} placeholder="Nhập tên Thông Báo"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Mô Tả Thông Báo</label>
                        <div>
                          <input onChange={changeModal} type="text" id="noti_desc" class="form-control input-lg" name="noti_desc" defaultValue={modal.noti_desc} placeholder="Nhập mô tả thông báo"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                      <label class="control-label">Khách Nhận</label>
                        <div>
                            <select onChange={changeModal} id="noti_cus_id" class="form-control input-lg" name="noti_cus_id">
                                {renderSelectKhach(modal.noti_cus_id)}
                            </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='row mt-3'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Thợ Nhận</label>
                        <div>
                          <select onChange={changeModal} id="noti_tho_id" class="form-control input-lg" name="noti_tho_id" >{renderSelectTho(modal.noti_tho_id)}</select>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Trạng Thái</label>
                        <div>
                            <select onChange={changeModal} id="noti_status" class="form-control input-lg" name="noti_status">
                            {renderSelectStatus(modal.noti_status)}
                            </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                  <button onClick={handleCreateUpdate} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
                </div>
              </div>
            </div>
          </div>
        )
    }
  return (
    <div id='thongBao'>
        <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Danh Sách Thông Báo</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
          </div>
            {renderThongBao()}
            {renderModal()}
        </div>
        <ToastContainer></ToastContainer>
    </div>
  )
}
