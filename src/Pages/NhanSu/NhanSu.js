import React, { useEffect, useState } from 'react'
import icon_add from "../../isset/img/icon/ICON ADD.png"
import icon_edit from "../../isset/img/icon/Báo cáo tổng-01.png"
import icon_delete from "../../isset/img/icon/ICON REMOVE.png"
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { sessionService } from '../../services/sessionService/SessionService';
import { nhanSuService } from '../../services/nhanSuService/nhanSuService';
import moment from 'moment';
import { Popconfirm } from 'antd';

export default function NhanSu() {
  let user = sessionService.getUser();
  let access_page = JSON.parse(user.access_page);
  let action_page = JSON.parse(user.action_page);
  useEffect(() => {
      nhanSuService.getUserList(user.token).then((res) => {
              setNhanSuArray(res.data);
            })
            .catch((err) => {
             console.log(err);
            });
  },[])

  let [nhanSuArray,setNhanSuArray] = useState([]);
  let [modal,setModal] = useState({});
  let [isUpdate,setIsUpdate] = useState(false);

  let handleSetModal = (id) => {
    if(id == -1){
      let tmpModal = {
        user_username:"",
        user_phone:"",
        user_password:"",
        user_fullname:"",
        user_status: 0,
        access_page: [{"access_id":"1","access_name":"App Khách","access_title":"app-khach","access_status":false},{"access_id":"2","access_name":"App Thợ","access_title":"app-tho","access_status":false},{"access_id":"3","access_name":"Đơn Hàng","access_title":"don-hang","access_status":false},{"access_id":"4","access_name":"Dịch Vụ","access_title":"dich-vu","access_status":false},{"access_id":"5","access_name":"Quảng Cáo","access_title":"quang-cao","access_status":false},{"access_id":"6","access_name":"Thông Báo","access_title":"thong-bao","access_status":false},{"access_id":"7","access_name":"Thống Kê Báo Cáo","access_title":"bao-cao","access_status":false},{"access_id":"8","access_name":"Tài Khoản Nhân Viên","access_title":"nhan-vien","access_status":false},{"access_id":"9","access_name":"Hệ Thống Thanh Toán","access_title":"thanh-toan","access_status":false},{"access_id":"10","access_name":"Danh Mục","access_title":"danh-muc","access_status":false},
        {"access_id":"11","access_name":"Khuyến Mãi","access_title":"khuyen-mai","access_status":false},{"access_id":"12","access_name":"Bảo Hành","access_title":"bao-hanh","access_status":false},{"access_id":"13","access_name":"Đánh Giá","access_title":"danh-gia","access_status":false}],
        action_page: [{"access_id":"1","action_create":false,"action_update":false,"action_delete":false},{"access_id":"2","action_create":false,"action_update":false,"action_delete":false},{"access_id":"3","action_create":false,"action_update":false,"action_delete":false},{"access_id":"4","action_create":false,"action_update":false,"action_delete":false},{"access_id":"5","action_create":false,"action_update":false,"action_delete":false},{"access_id":"6","action_create":false,"action_update":false,"action_delete":false},{"access_id":"7","action_create":false,"action_update":false,"action_delete":false},{"access_id":"8","action_create":false,"action_update":false,"action_delete":false},{"access_id":"9","action_create":false,"action_update":false,"action_delete":false},{"access_id":"10","action_create":false,"action_update":false,"action_delete":false},{"access_id":"11","action_create":false,"action_update":false,"action_delete":false},{"access_id":"12","action_create":false,"action_update":false,"action_delete":false},{"access_id":"13","action_create":false,"action_update":false,"action_delete":false}]
      }
      setModal(tmpModal);
      setIsUpdate(false);
    }else{
      let access = JSON.parse(nhanSuArray[id]?.access_page);
      access.map((item) => {
        if(item.access_status == 1){
          item.access_status = true;
        }else{
          item.access_status = false;
        }
      })
      let action = JSON.parse(nhanSuArray[id]?.action_page);
      action.map((item) => {
        if(item.action_create == 1){
          item.action_create = true;
        }else{
          item.action_create = false;
        };
        if(item.action_update == 1){
          item.action_create = true;
        }else{
          item.action_update = false;
        };
        if(item.action_delete == 1){
          item.action_create = true;
        }else{
          item.action_delete = false;
        };
      })
      setModal({...nhanSuArray[id],access_page:access,action_page:action});
      setIsUpdate(true);
    }
  }
  let handleCreateUpdate = () => {
    if(isUpdate){
      if(modal.user_fullname.length == 0 ){
        toast.error("Vui lòng nhập họ tên người dùng",{position: toast.POSITION.TOP_CENTER});
        return;
      };
      nhanSuService.updateUser(user.token,modal).then((res) => {
        toast.success("Cập nhật tài khoản thành công",{position: toast.POSITION.TOP_CENTER});
        window.location.reload();
            })
            .catch((err) => {
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }else{
      if(modal.user_username.length <6 ){
        toast.error("Tài khoản phải ít nhất 6 ký tự",{position: toast.POSITION.TOP_CENTER});
        return;
      };
      if(modal.user_password.length <8 ){
        toast.error("Mật Khẩu phải ít nhất 8 ký tự",{position: toast.POSITION.TOP_CENTER});
        return;
      };
      if(modal.user_fullname.length == 0 ){
        toast.error("Vui lòng nhập họ tên người dùng",{position: toast.POSITION.TOP_CENTER});
        return;
      };
      nhanSuService.createUser(user.token,modal).then((res) => {
        toast.success("Tạo tài khoản thành công",{position: toast.POSITION.TOP_CENTER});
        window.location.reload();
            })
            .catch((err) => {
              console.log(err);
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }
}
let handleDeleteUser = (id) => {
  nhanSuService.updateUser(user.token,{user_id:id,user_status:0}).then((res) => {
      toast.success("Xoá tài khoản thành công",{position: toast.POSITION.TOP_CENTER});
      window.location.reload();
        })
        .catch((err) => {
          toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
        });
}
  let changeModal = (e) => {
    let modal1 = {...modal};
    if(e.target.name == "access_status"){
      modal1.access_page[e.target.id][e.target.name] = e.target.checked?1:0;
      setModal(modal1);
    }else if(e.target.name == "action_update" || e.target.name == "action_create" || e.target.name == "action_delete"){
      modal1.action_page[e.target.id][e.target.name] = e.target.checked?1:0;
      setModal(modal1);
    }else{
      modal[e.target.name] = e.target.value;
    }
}
  let renderAddButton = () => {
    if(action_page[8].action_create == 1){
      return <button onClick={() => handleSetModal(-1)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0"}}>
        <img width={"20px"} height={"20px"} src={icon_add} alt="" />
      </button>
    }
  }
  let renderEditButton = (id) => {
    if(action_page[8].action_update == 1){
      return <button onClick={() => handleSetModal(id)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0",background: "transparent",marginRight:"7px"}}>
        <img width={"20px"} height={"20px"} src={icon_edit} alt="" />
      </button>
    }
  }
  let renderDeleteButton = (id) => {
    if(action_page[1].action_delete == 1){
      return <Popconfirm
      title="Xoá Tài Khoản"
      description="Bạn Chắc Chắn xoá người dùng này?"
      okText="Xoá"
      cancelText="Huỷ"
      onConfirm={() => {handleDeleteUser(id)}}
    >
      <button style={{border:"none",padding:"0",background: "transparent"}}>
        <img width={"20px"} height={"20px"} src={icon_delete} alt="" />
      </button>
    </Popconfirm>
    }
  }
  let renderSelectStatus = (status) => {
    let status_array = ["Bị Khoá","Hoạt Động"];
    return status_array.map((item,index) => {
      if(index == status){
        return <option selected value={index}>{status_array[index]}</option>
      }else{
        return <option value={index}>{status_array[index]}</option>
      }
    })
  }
  let renderQuyenQuanTri = (nhanSu) => {
      let access = JSON.parse(nhanSu?.access_page);
      let action = JSON.parse(nhanSu?.action_page);
      let length = access.length;
      return access.map((item,index) => {
        let html = "";
        if(item.access_status == 1){
          html += item.access_name + ": Xem";
          if(action[index].action_create == 1){
            html += ", Thêm";
          }
          if(action[index].action_update == 1){
            html += ", Sửa";
          }
          if(action[index].action_delete == 1){
            html += ", Xoá";
          }
          return <p>{html}</p>;
        }
      })
  }
  let renderStatus = (status) => {
    if(status == 1){
      return <p>Hoạt Động</p>
    }else{
      return <p className='text-danger'>Bị Khoá</p>
    }
  }
  let renderNhanSuItem = () => {
    return nhanSuArray?.map((nhanSu,index) => {
      return(
        <tr>
          <td><p>{index + 1}</p></td>
          <td>{moment(nhanSu.user_create_date).format("HH:mm:ss DD/MM/YYYY")}</td>
          <td>{nhanSu.user_username}</td>
          <td>{nhanSu.user_fullname}</td>
          <td>{nhanSu.user_phone}</td>
          <td>{renderQuyenQuanTri(nhanSu)}</td>
          <td>{renderStatus(nhanSu.user_status)}</td>
          <td>{renderEditButton(index)}{renderDeleteButton(nhanSu.user_id)}</td>
        </tr>
      )
    })
  }
  let renderNhanSu = () => {
      if(access_page[8].access_status == 0){
        return(
          <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
        )
      }else{
        return(
          <>
            <table class="table table-bordered">
              <thead>
                <th style={{width:"40px"}}>{renderAddButton()}</th>
                <th>Ngày Đăng Ký</th>
                <th>Tên Tài Khoản</th>
                <th>Tên Người Dùng</th>
                <th>Số Điện Thoại</th>
                <th>Quyền Quản Trị</th>
                <th>Trạng Thái</th>
                <th style={{width:"90px"}}>Thao Tác</th>
              </thead>
              <tbody id='myTable'>
                  {renderNhanSuItem()}
              </tbody>
            </table>
          </>
        )
      }
    }
    let renderTable1 = (modal) => {
      return modal.access_page?.map((page,index) => {
        if(index >= 6){
          return;
        }
        return <tr>
          <td>{page.access_name}</td>
          <td>{<input onChange={changeModal} id={index} name='access_status' checked = {page.access_status} type='checkbox' />}</td>
          <td>{<input onChange={changeModal} id={index} name='action_create'  checked={modal.action_page[index].action_create} type='checkbox' />}</td>
          <td>{<input onChange={changeModal} id={index} name='action_update' checked={modal.action_page[index].action_update} type='checkbox' />}</td>
          <td>{<input onChange={changeModal} id={index} name='action_delete' checked={modal.action_page[index].action_delete} type='checkbox' />}</td>
        </tr>
      })
    }
    let renderTable2 = (modal) => {
      return modal.access_page?.map((page,index) => {
        if(index < 6){
          return;
        }
        return <tr>
          <td>{page.access_name}</td>
          <td>{<input onChange={changeModal} id={index} name='access_status' checked={page.access_status} type='checkbox' />}</td>
          <td>{<input onChange={changeModal} id={index} name='action_create' checked={modal.action_page[index].action_create} type='checkbox' />}</td>
          <td>{<input onChange={changeModal} id={index} name='action_update' checked={modal.action_page[index].action_update} type='checkbox' />}</td>
          <td>{<input onChange={changeModal} id={index} name='action_delete' checked={modal.action_page[index].action_delete} type='checkbox' />}</td>
        </tr>
      })
    }
    let renderModal = () => {
      return(
        <div class="modal fade modal-xl" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div style={{background:"#037ec1"}} class="modal-header">
                <div></div>
                <h1 class="modal-title fs-2 text-light" id="exampleModalLabel">Thêm Tài Khoản Nhân Sự</h1>
                <button style={{marginLeft:"0"}} type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <div className='row'>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Tên Người Dùng</label>
                      <div>
                        <input defaultValue={modal.user_fullname} onChange={changeModal} type="text" id="user_fullname" class="form-control input-lg" name="user_fullname"  placeholder="Nhập họ tên người dùng"></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Số Điện Thoại</label>
                      <div>
                        <input defaultValue={modal.user_phone} onChange={changeModal} type="text" id="user_phone" class="form-control input-lg" name="user_phone"  placeholder="Nhập Số điện thoại người dùng"></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Tài Khoản</label>
                      <div>
                        <input defaultValue={modal.user_username} onChange={changeModal} type="text" id="user_username" class="form-control input-lg" name="user_username"  placeholder="Nhập tài khoản người dùng"></input>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='row mt-3'>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Mật Khẩu</label>
                      <div>
                        <input defaultValue={modal.user_password} onChange={changeModal} type="password" id="user_password" class="form-control input-lg" name="user_password"  placeholder="Nhập Mật khẩu người dùng"></input>
                      </div>
                    </div>
                  </div>
                  <div className='col-4'>
                    <div className='form-group'>
                      <label class="control-label">Trạng Thái</label>
                      <div>
                      <select onChange={changeModal} id="user_status" class="form-control input-lg" name="user_status">
                          {renderSelectStatus(modal.user_status)}
                      </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='row mt-3'>
                  <div className='col-6'>
                    <table className='table table-bordered'>
                      <thead>
                        <th></th>
                        <th>Xem</th>
                        <th>Thêm</th>
                        <th>Sửa</th>
                        <th>Xoá</th>
                      </thead>
                      <tbody>
                          {renderTable1(modal)}
                      </tbody>
                    </table>
                  </div>
                  <div className='col-6'>
                  <table className='table table-bordered'>
                      <thead>
                        <th></th>
                        <th>Xem</th>
                        <th>Thêm</th>
                        <th>Sửa</th>
                        <th>Xoá</th>
                      </thead>
                      <tbody>
                          {renderTable2(modal)}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                <button onClick={handleCreateUpdate} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
              </div>
            </div>
          </div>
        </div>
      )
    }
  return (
    <div id='nhanSu'>
        <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Danh Sách Tài Khoản</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input autoComplete='off' placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
          </div>
          {renderNhanSu()}
          {renderModal()}
      </div>
      <ToastContainer></ToastContainer>
    </div>
  )
}
