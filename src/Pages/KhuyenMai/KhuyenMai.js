import React, { useEffect, useState } from 'react'
import { khuyenMaiService } from '../../services/khuyenMaiService/khuyenMaiService';
import { sessionService } from '../../services/sessionService/SessionService';
import moment from 'moment';
import { Popconfirm } from 'antd';
import icon_add from "../../isset/img/icon/ICON ADD.png"
import icon_edit from "../../isset/img/icon/Báo cáo tổng-01.png"
import icon_delete from "../../isset/img/icon/ICON REMOVE.png"
import { ToastContainer, toast } from 'react-toastify';

export default function KhuyenMai() {
    let user = sessionService.getUser();
    let access_page = JSON.parse(user.access_page);
    let action_page = JSON.parse(user.action_page);
    let [modal,setModal] = useState({});
    let [isUpdate,setIsUpdate] = useState(false);
    let [khuyenMaiArray,setKhuyenMaiArray] = useState([]);
    let [danhMuc,setDanhMuc] = useState([]);
    useEffect(() => {
      khuyenMaiService.getKhuyenMai(user.token).then((res) => {
              setKhuyenMaiArray(res.data);
            })
            .catch((err) => {
             console.log(err);
            });
      khuyenMaiService.getDanhMuc(user.token).then((res) => {
              setDanhMuc(res.data.content);
            })
            .catch((err) => {
             console.log(err);
            });
    },[])

    let changeModal = (e) => {
        modal[e.target.name] = e.target.value;
    }
    let renderSelectDanhMuc = (danhmuc_id) => {
      let danhMuc_new = [...danhMuc,{danhmuc_id: 0,danhmuc_ten: "Tất Cả"}]
      return danhMuc_new.map((item,index) => {
        if(item.danhmuc_id == danhmuc_id){
          return <option selected value={item.danhmuc_id}>{item.danhmuc_ten}</option>
        }else{
          return <option value={item.danhmuc_id}>{item.danhmuc_ten}</option>
        }
      })
  }
    let renderSelectStatus = (status) => {
        let status_array = ["Đóng","Mở"];
        return status_array.map((item,index) => {
          if(index == status){
            return <option selected value={index}>{status_array[index]}</option>
          }else{
            return <option value={index}>{status_array[index]}</option>
          }
        })
    }
    let handleSetModal = (id) => {
        if(id == -1){
          let tmpModal = {
            promo_code:"",
            promo_type: "0",
            promo_tiengiam: "",
            promo_max: "",
            promo_start: "",
            promo_stop: "",
            promo_status:0,
            promo_desc: "",
            danhmuc_id: null
          }
          setModal(tmpModal);
          setIsUpdate(false);
        }else{
          setModal(khuyenMaiArray[id]);
          setIsUpdate(true);
        }
    }
    let renderSelectLoaiKM = (type) => {
        let type_array = ["Giảm Trực Tiếp","Giảm Theo Phần Trăm"];
        return type_array.map((item,index) => {
          if(index == type){
            return <option selected value={index}>{type_array[index]}</option>
          }else{
            return <option value={index}>{type_array[index]}</option>
          }
        })
    }
    const randomString = () => {
        const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let result = '';
        for (let i = 6; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
      }
    let handleCreateUpdate = () => {
        modal.promo_tiengiam = String(modal.promo_tiengiam);
        modal.promo_max = String(modal.promo_max);
        if(modal.promo_code == null || modal.promo_code == ""){
            modal.promo_code = randomString();
        }
        if(modal.promo_tiengiam == 0 || modal.promo_tiengiam == ""){
          toast.error("Vui lòng nhập số tiền giảm",{position: toast.POSITION.TOP_CENTER});
          return;
        }
        if(modal.promo_start == ""){
            toast.error("Vui lòng chọn ngày bắt đầu",{position: toast.POSITION.TOP_CENTER});
            return;
        }
        if(modal.promo_stop == ""){
            toast.error("Vui lòng chọn ngày kết thúc",{position: toast.POSITION.TOP_CENTER});
            return;
        }
        if(modal.promo_desc == ""){
            toast.error("Vui lòng nhập mô tả cho khuyến mãi",{position: toast.POSITION.TOP_CENTER});
            return;
          }
        if(isUpdate){
          khuyenMaiService.updateKhuyenMai(user.token,modal).then((res) => {
            toast.success("Cập nhật khuyến mãi thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
                })
                .catch((err) => {
                    console.log(err);
                  toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
                });
        }else{
          khuyenMaiService.createKhuyenMai(user.token,modal).then((res) => {
            toast.success("Tạo khuyến mãi thành công",{position: toast.POSITION.TOP_CENTER});
              window.location.reload();
          })
          .catch((err) => {
            console.log(err);
            toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
          });
        }
      }
    let handleDeleteKhuyenMai = (id) => {
        khuyenMaiService.updateKhuyenMai(user.token,{promo_id:id,promo_status:0}).then((res) => {
            toast.success("Xoá khuyến mãi thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
              })
              .catch((err) => {
                toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
              });
        }
    let renderAddButton = () => {
        if(action_page[10]?.action_create == 1){
          return <button onClick={() => handleSetModal(-1)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0"}}>
            <img width={"20px"} height={"20px"} src={icon_add} alt="" />
          </button>
        }
      }
      let renderEditButton = (id) => {
        if(action_page[10].action_update == 1){
          return <button onClick={() => handleSetModal(id)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0",background: "transparent",marginRight:"7px"}}>
            <img width={"20px"} height={"20px"} src={icon_edit} alt="" />
          </button>
        }
      }
      let renderDeleteButton = (id) => {
        if(action_page[10].action_delete == 1){
          return <Popconfirm
          title="Xoá Thanh Toán"
          description="Bạn Chắc chắn xoá Thanh Toán này?"
          okText="Xoá"
          cancelText="Huỷ"
          onConfirm={() => {handleDeleteKhuyenMai(id)}}
        >
          <button style={{border:"none",padding:"0",background: "transparent"}}>
            <img width={"20px"} height={"20px"} src={icon_delete} alt="" />
          </button>
        </Popconfirm>
        }
    }
    let renderKhuyenMaiItem = () => {
        return khuyenMaiArray.map((khuyenMai,index) => {
          return <tr>
            <td>{index+1}</td>
            <td>{khuyenMai.promo_code}</td>
            <td>{khuyenMai.promo_type==0?<p>Giảm Trực Tiếp</p>:<p>Giảm Theo Phần Trăm</p>}</td>
            <td>{new Intl.NumberFormat('en-Us').format(khuyenMai.promo_tiengiam)}</td>
            <td>{khuyenMai.promo_max==0?<p>Không Giới Hạn</p>:new Intl.NumberFormat('en-Us').format(khuyenMai.promo_max)}</td>
            <td>{khuyenMai.promo_desc}</td>
            <td>{moment(khuyenMai.promo_start).format("HH:mm:ss DD-MM-YYYY")}</td>
            <td>{moment(khuyenMai.promo_stop).format("HH:mm:ss DD-MM-YYYY")}</td>
            <td>{["",null].includes(khuyenMai.danhmuc_id)?"Tất Cả":khuyenMai.danhmuc.danhmuc_ten}</td>
            <td>{khuyenMai.promo_status==1?<p className='text-success'>Hoạt Động</p>:<p className='text-danger'>Ngưng</p>}</td>
            <td>{renderEditButton(index)}</td>
          </tr>
        })
    }
    let renderKhuyenMai = () => {
        if(access_page[10].access_status == 0){
          return(
            <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
          )
        }else{
          return(
            <>
              <table class="table table-bordered">
                <thead>
                  <th style={{width:"40px"}}>{renderAddButton(-1)}</th>
                  <th>Mã Khuyến Mãi</th>
                  <th>Loại Khuyến Mãi</th>
                  <th>Số Tiền/Phần Trăm Giảm</th>
                  <th>Số Tiền Giảm Tối Đa</th>
                  <th>Mô Tả</th>
                  <th>Ngày Bắt Đầu</th>
                  <th>Ngày Kết Thúc</th>
                  <th>Danh Mục</th>
                  <th>Trạng Thái</th>
                  <th>Thao Tác</th>
                </thead>
                <tbody id='myTable'>
                    {renderKhuyenMaiItem()}
                </tbody>
              </table>
            </>
          )
      }
    }
    let renderModal = () => {
        return(
          <div class="modal fade modal-xl" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div style={{background:"#037ec1"}} class="modal-header">
                  <div></div>
                  <h1 class="modal-title fs-2 text-light" id="exampleModalLabel">Thêm Khuyến Mãi</h1>
                  <button style={{marginLeft:"0"}} type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <div className='row'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Mã Khuyến Mãi</label>
                        <div>
                          <input onChange={changeModal} type="text" id="promo_code" class="form-control input-lg" name="promo_code" defaultValue={modal.promo_code} placeholder="Để trống nếu muốn tạo ngẫu nhiên"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Loại Khuyến Mãi</label>
                        <div>
                          <select onChange={changeModal} name="promo_type" id="promo_type" class="form-control input-lg">
                                {renderSelectLoaiKM(modal.promo_type)}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                        <div className='form-group'>
                            <label class="control-label">Giá Tiền/Phần Trăm Giảm</label>
                            <div>
                                <input onChange={changeModal} type="text" id="promo_tiengiam" class="form-control input-lg" name="promo_tiengiam" defaultValue={modal.promo_tiengiam} placeholder="Số Tiền Giảm"></input>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div className='row mt-3'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Số Tiền Giảm Tối Đa</label>
                        <div>
                          <input onChange={changeModal} type="text" id="promo_max" class="form-control input-lg" name="promo_max" defaultValue={modal.promo_max} placeholder="Để trống nếu không giới hạn"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Thời Gian Bắt Đầu</label>
                        <div>
                            <input onChange={changeModal} type="datetime-local" id="promo_start" class="form-control input-lg" name="promo_start" defaultValue={modal.promo_start}></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                        <div className='form-group'>
                            <label class="control-label">Thời Gian Kết Thúc</label>
                            <div>
                                <input onChange={changeModal} type="datetime-local" id="promo_stop" class="form-control input-lg" name="promo_stop" defaultValue={modal.promo_stop}></input>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div className='row mt-3'>
                    <div className='col-4'>
                        <div className='form-group'>
                            <label class="control-label">Mô Tả Khuyến Mãi</label>
                            <div>
                                <input onChange={changeModal} type="text" id="promo_desc" class="form-control input-lg" name="promo_desc" defaultValue={modal.promo_desc}></input>
                            </div>
                        </div>
                    </div>
                    <div className='col-4'>
                        <div className='form-group'>
                            <label class="control-label">Danh Mục Khuyến Mãi</label>
                            <div>
                                <select onChange={changeModal} id="danhmuc_id" class="form-control input-lg" name="danhmuc_id">
                                        {renderSelectDanhMuc(modal.danhmuc_id?modal.danhmuc_id:0)}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className='col-4'>
                            <div className='form-group'>
                                <label class="control-label">Trạng Thái</label>
                                <div>
                                    <select onChange={changeModal} id="promo_status" class="form-control input-lg" name="promo_status">
                                        {renderSelectStatus(modal.promo_status)}
                                    </select>
                                </div>
                            </div>
                        </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                  <button onClick={handleCreateUpdate} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
                </div>
              </div>
            </div>
          </div>
        )
    }
  return (
    <div id='khuyenMai'>
      <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Danh Sách Khuyến Mãi</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input autoComplete='off' placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
          </div>
          {renderKhuyenMai()}
          {renderModal()}
        </div>
        <ToastContainer></ToastContainer>
    </div>
  )
}
