import React, { useEffect, useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { sessionService } from '../../services/sessionService/SessionService';
import {appThoService} from "../../services/appThoService/appThoService";
import icon_add from "../../isset/img/icon/ICON ADD.png"
import icon_edit from "../../isset/img/icon/Báo cáo tổng-01.png"
import icon_delete from "../../isset/img/icon/ICON REMOVE.png"
import moment from 'moment';
import { Popconfirm } from 'antd';
import { dichVuService } from '../../services/dichVuService/dichVuService';

export default function AppTho() {
    let user = sessionService.getUser();
    let access_page = JSON.parse(user.access_page);
    let action_page = JSON.parse(user.action_page);
    let [modal,setModal] = useState({});
    let [userList,setUserList] = useState([]);
    let [userListClone,setUserListCLone] = useState([]);
    let [isUpdate,setIsUpdate] = useState(false);
    let [danhmuc,setDanhMuc] = useState([]);

    useEffect(() => {
        appThoService.getUserList(user.token).then((res) => {
                setUserList(res.data);
                setUserListCLone(res.data);
              })
              .catch((err) => {
                console.log(err);
              });
        dichVuService.getDanhMuc(user.token).then((res) => {
                setDanhMuc(res.data.content)
              })
              .catch((err) => {
               console.log(err);
              });
    },[])
    let sortArray = (e) => {
      let array = userListClone.slice()
      if(e.target.value == 0){
        array.sort(function(a,b){
          var nameA = a.create_date;
          var nameB = b.create_date;
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }

          // name trùng nhau
          return 0;
        });
      }else{
        array.sort(function(a,b){
          var nameA = a.create_date;
          var nameB = b.create_date;
          if (nameA > nameB) {
            return -1;
          }
          if (nameA < nameB) {
            return 1;
          }

          // name trùng nhau
          return 0;
        });
      }
      setUserList(array);
    }
    let renderStatus = (status) => {
        if(status == 1){
          return <p>Hoạt Động</p>
        }else{
          return <p className='text-danger'>Bị Khoá</p>
        }
    }
    let renderDanhmuc = (id) => {
      let index = danhmuc?.findIndex((item) => {
        return item?.danhmuc_id == id;
      })
      return danhmuc[index]?.danhmuc_ten;
    }
    let renderSelectStatus = (status) => {
        let status_array = ["Bị Khoá","Hoạt Động"];
        return status_array.map((item,index) => {
          if(index == status){
            return <option selected value={index}>{status_array[index]}</option>
          }else{
            return <option value={index}>{status_array[index]}</option>
          }
        })
    }
    let renderSelectDanhMuc = (id) => {
      return danhmuc?.map((item,index) => {
        if(item.danhmuc_id == id){
          return <option selected value={item?.danhmuc_id}>{item?.danhmuc_ten}</option>
        }else{
          return <option value={item?.danhmuc_id}>{item?.danhmuc_ten}</option>
        }
      })
  }
    let renderAddButton = () => {
        if(action_page[1].action_create == 1){
          return <button onClick={() => handleSetModal(-1)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0"}}>
            <img width={"20px"} height={"20px"} src={icon_add} alt="" />
          </button>
        }
    }
    let renderEditButton = (id) => {
      if(action_page[1].action_update == 1){
        return <button onClick={() => handleSetModal(id)} data-bs-toggle="modal" data-bs-target="#updateModal" style={{border:"none",padding:"0",background: "transparent",marginRight:"7px"}}>
          <img width={"20px"} height={"20px"} src={icon_edit} alt="" />
        </button>
      }
    }
    let renderDeleteButton = (id) => {
      if(action_page[1].action_delete == 1){
        return <Popconfirm
        title="Xoá Tài Khoản"
        description="Bạn Chắc Chắn xoá người dùng này?"
        okText="Xoá"
        cancelText="Huỷ"
        onConfirm={() => {handleDeleteUser(id)}}
      >
        <button style={{border:"none",padding:"0",background: "transparent"}}>
          <img width={"20px"} height={"20px"} src={icon_delete} alt="" />
        </button>
      </Popconfirm>
      }
    }
    let changeModal = (e) => {
        modal[e.target.name] = e.target.value;
    }
    let handleSetModal = (id) => {
        if(id == -1){
          let tmpModal = {
            user_username:"",
            user_phone:"",
            user_password:"",
            user_email:"",
            user_address:"",
            user_fullname:"",
            user_status: 0,
            user_danhmuc:1
          }
          setModal(tmpModal);
          setIsUpdate(false);
        }else{
          setModal(userList[id]);
          setIsUpdate(true);
        }
    }
    let handleCreateUpdate = () => {
        if(isUpdate){
          if(modal.user_fullname.length == 0 ){
            toast.error("Vui lòng nhập họ tên người dùng",{position: toast.POSITION.TOP_CENTER});
            return;
          };
          appThoService.updateUser(modal,user.token).then((res) => {
            toast.success("Cập nhật tài khoản thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
                })
                .catch((err) => {
                  toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
                });
        }else{
          if(modal.user_username.length <6 ){
            toast.error("Tài khoản phải ít nhất 6 ký tự",{position: toast.POSITION.TOP_CENTER});
            return;
          };
          if(modal.user_password.length <8 ){
            toast.error("Mật Khẩu phải ít nhất 8 ký tự",{position: toast.POSITION.TOP_CENTER});
            return;
          };
          if(modal.user_fullname.length == 0 ){
            toast.error("Vui lòng nhập họ tên người dùng",{position: toast.POSITION.TOP_CENTER});
            return;
          };
          appThoService.createUser(modal,user.token).then((res) => {
            toast.success("Tạo tài khoản thành công",{position: toast.POSITION.TOP_CENTER});
            window.location.reload();
                })
                .catch((err) => {
                  toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
                });
        }
    }
    let handleDeleteUser = (id) => {
      appThoService.updateUser({user_id:id,user_status:0},user.token).then((res) => {
          toast.success("Xoá tài khoản thành công",{position: toast.POSITION.TOP_CENTER});
          window.location.reload();
            })
            .catch((err) => {
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }
    let handleUploadAnh = (user_id) => {
      let update_type;
      let files1 = document.getElementsByName("user_img")[0].files[0];
      let files2 = document.getElementsByName("user_img")[1].files[0];
      // 0: update cả 2 mặt, 1 là mặt trước ,2 là mặt sau
      if(files1 && files2){
        update_type = 0;
      }else if(files1){
        update_type = 1;
      }else{
        update_type = 2;
      }
      appThoService.updateCCCD(files1,files2,user_id,update_type,user.token).then((res) => {
        toast.success("Upload Hình Thành Công",{position: toast.POSITION.TOP_CENTER});
        window.location.reload();
            })
            .catch((err) => {
              console.log(err);
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }
    let handleUploadAvatar = (user_id) => {
      let files = document.getElementsByName("user_avatar")[0].files[0];
      appThoService.updateAvatar(files,user_id,user.token).then((res) => {
        toast.success("Upload Hình Thành Công",{position: toast.POSITION.TOP_CENTER});
        window.location.reload();
            })
            .catch((err) => {
              console.log(err);
              toast.error(err.response.data,{position: toast.POSITION.TOP_CENTER});
            });
    }
    let renderUser = () => {
        return userList?.map((user,index) => {
          return(
            <tr>
              <td><p>{index + 1}</p></td>
              <td>{moment(user.create_date).format("HH:mm:ss DD/MM/YYYY")}</td>
              <td>{user.user_username}</td>
              <td>{user.user_fullname}</td>
              <td>{user.user_phone}</td>
              <td>{user.user_address}</td>
              <td>{user.user_email}</td>
              <td><button onClick={() => handleSetModal(index)} data-bs-toggle="modal" data-bs-target="#modalUpload" type="button" className='border-0 bg-body text-decoration-underline text-primary'>Quản Lý</button></td>
              <td><button onClick={() => handleSetModal(index)} data-bs-toggle="modal" data-bs-target="#modalAvatar" type="button" className='border-0 bg-body text-decoration-underline text-primary'>Quản Lý</button></td>
              <td>{user.user_finishtime}</td>
              <td>{renderDanhmuc(user.user_danhmuc)}</td>
              <td>{renderStatus(user.user_status)}</td>
              <td>{renderEditButton(index)}{renderDeleteButton(user.user_id)}</td>
            </tr>
          )
        })
      }
    let renderAppTho = () => {
        if(access_page[1].access_status == 0){
          return(
            <h2>Bạn Không có Quyền Truy Cập Trang Này</h2>
          )
        }else{
          return(
            <>
              <table class="table table-bordered">
                <thead>
                  <th style={{width:"40px"}}>{renderAddButton()}</th>
                  <th>Ngày Đăng Ký</th>
                  <th>Tên Tài Khoản</th>
                  <th>Tên Người Dùng</th>
                  <th>Số Điện Thoại</th>
                  <th>Địa Chỉ</th>
                  <th>Email</th>
                  <th>CCCD</th>
                  <th>Hình Ảnh</th>
                  <th>Số Lần Hoàn Thành</th>
                  <th>Lĩnh Vực</th>
                  <th>Trạng Thái</th>
                  <th style={{width:"90px"}}>Thao Tác</th>
                </thead>
                <tbody id='myTable'>
                    {renderUser()}
                </tbody>
              </table>
            </>
          )
        }
      }
      let renderModal = () => {
        return(
          <div class="modal fade modal-xl" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div style={{background:"#037ec1"}} class="modal-header">
                  <div></div>
                  <h1 class="modal-title fs-2 text-light" id="exampleModalLabel">Thêm Tài Khoản App Thợ</h1>
                  <button style={{marginLeft:"0"}} type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <div className='row'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Tên Người Dùng</label>
                        <div>
                          <input onChange={changeModal} type="text" id="user_fullname" class="form-control input-lg" name="user_fullname" defaultValue={modal.user_fullname} placeholder="Nhập họ tên người dùng"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Số Điện Thoại</label>
                        <div>
                          <input onChange={changeModal} type="text" id="user_phone" class="form-control input-lg" name="user_phone" defaultValue={modal.user_phone} placeholder="Nhập Số điện thoại người dùng"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Email</label>
                        <div>
                          <input onChange={changeModal} type="text" id="user_email" class="form-control input-lg" name="user_email" defaultValue={modal.user_email} placeholder="Nhập Email người dùng"></input>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='row mt-3'>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Địa Chỉ</label>
                        <div>
                          <input onChange={changeModal} type="text" id="user_address" class="form-control input-lg" name="user_address" defaultValue={modal.user_address} placeholder="Nhập địa chỉ người dùng"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Tài Khoản</label>
                        <div>
                          <input onChange={changeModal} type="text" id="user_username" class="form-control input-lg" name="user_username" defaultValue={modal.user_username} placeholder="Nhập tài khoản người dùng"></input>
                        </div>
                      </div>
                    </div>
                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Mật Khẩu</label>
                        <div>
                          <input onChange={changeModal} type="password" id="user_password" class="form-control input-lg" name="user_password" defaultValue={modal.user_password} placeholder="Nhập Mật khẩu người dùng"></input>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='row mt-3'>
                  <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Lĩnh Vực</label>
                        <div>
                        <select onChange={changeModal} id="user_danhmuc" class="form-control input-lg" name="user_danhmuc">
                            {renderSelectDanhMuc(modal.user_danhmuc)}
                        </select>
                        </div>
                      </div>
                    </div>

                    <div className='col-4'>
                      <div className='form-group'>
                        <label class="control-label">Trạng Thái</label>
                        <div>
                        <select onChange={changeModal} id="user_status" class="form-control input-lg" name="user_status">
                            {renderSelectStatus(modal.user_status)}
                        </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                  <button onClick={handleCreateUpdate} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
                </div>
              </div>
            </div>
          </div>
        )
      }
      
      let renderModalUpload = () => {
        return(
          <div class="modal fade modal-xl" id="modalUpload" tabindex="-1" aria-labelledby="modalUploadLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  
                  <h1 class="modal-title fs-5" id="modalUploadLabel">Quản Lý Ảnh</h1>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div className='row'>
                      <div className='col-6'>
                        <div>
                          <a target="_blank" href={modal?.user_img1}><img src={modal?.user_img1} width={"90%"} alt="" /></a>
                        </div>
                      </div>
                      <div className='col-6'>
                        <div>
                          <a target="_blank" href={modal?.user_img2}><img src={modal?.user_img2} width={"90%"} alt="" /></a>
                        </div>
                      </div>
                    </div>
                    <div className='upload mt-3'>
                      <h5>Thay Đổi Ảnh</h5>
                      <div>
                        <span>CCCD Mặt Trước: </span><input className="form-control"
                          type="file" accept='image/*'
                          name = "user_img"
                          />
                      </div>
                      <div className='mt-3'>
                        <span>CCCD Mặt Sau: </span><input className="form-control"
                          type="file" accept='image/*'
                          name = "user_img"
                          />
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                  <button onClick={() => handleUploadAnh(modal.user_id)} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
                </div>
              </div>
            </div>
          </div>
        )
      }
      let renderModalAvatar = () => {
        return(
          <div class="modal fade" id="modalAvatar" tabindex="-1" aria-labelledby="modalAvatarLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h1 class="modal-title fs-5" id="modalAvatarLabel">Quản Lý Ảnh</h1>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div className='row'>
                        <div>
                          <a target="_blank" href={modal?.user_avatar}><img src={modal?.user_avatar} width={"90%"} alt="" /></a>
                        </div>
                    </div>
                    <div className='upload mt-3'>
                      <h5>Thay Đổi Ảnh</h5>
                      <div className='mt-3'>
                        <input className="form-control"
                          type="file" accept='image/*'
                          name = "user_avatar"
                          />
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                  <button onClick={() => handleUploadAvatar(modal.user_id)} type="button" class="btn btn-primary">Lưu Thay Đổi</button>
                </div>
              </div>
            </div>
          </div>
        )
      }
  return (
    <div id='appTho'>
      <div className='p-2'>
          <h3 className='text-sm-start fw-normal'>Danh Sách Tài Khoản</h3>
          <div class="input-group mb-3">
            <div class="form-outline">
              <input autoComplete='off' placeholder='Tìm kiếm...' type="search" id="myInput" class="form-control rounded-end-0" />
            </div>
            <button disabled type="button" class="btn btn-primary">
              <i class="fas fa-search"></i>
            </button>
            <select onChange={sortArray} style={{marginLeft:"20px",borderRadius:"5px",borderColor:"#d5d5d5"}} name="" id="">
              <option selected value="0">Ngày Đăng Ký Xa Nhất</option>
              <option value="1">Ngày Đăng Ký Gần Nhất</option>
            </select>
          </div>
          {renderAppTho()}
          {renderModal()}
          {renderModalUpload()}
          {renderModalAvatar()}
      </div>
      <ToastContainer></ToastContainer>
    </div>
  )
}
