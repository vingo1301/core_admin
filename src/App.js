import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LoginPage from './Pages/LoginPage/LoginPage';
import HOC from './HOC/HOC'
import AppKhach from './Pages/AppKhach/AppKhach'
import AppTho from './Pages/AppTho/AppTho';
import DichVu from './Pages/dichVu/DichVu';
import DonHang from './Pages/DonHang/DonHang';
import QuangCao from './Pages/QuangCao/QuangCao';
import ThongBao from './Pages/ThongBao/ThongBao';
import ThongKe from './Pages/ThongKe/ThongKe';
import ThanhToan from './Pages/ThanhToan/ThanhToanNew';
import NhanSu from './Pages/NhanSu/NhanSu';
import DanhMuc from './Pages/DanhMuc/DanhMuc';
import KhuyenMai from './Pages/KhuyenMai/KhuyenMai';
import BaoHanh from './Pages/BaoHanh/BaoHanh';
import DanhGia from './Pages/DanhGia/DanhGia';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
        <Route path='/' element={<LoginPage></LoginPage>}></Route>
        <Route path='/app-khach' element={<HOC Component={AppKhach}></HOC>}></Route>
        <Route path='/app-tho' element={<HOC Component={AppTho}></HOC>}></Route>
        <Route path='/dich-vu' element={<HOC Component={DichVu}></HOC>}></Route>
        <Route path='/don-hang' element={<HOC Component={DonHang}></HOC>}></Route>
        <Route path='/quang-cao' element={<HOC Component={QuangCao}></HOC>}></Route>
        <Route path='/thong-bao' element={<HOC Component={ThongBao}></HOC>}></Route>
        <Route path='/bao-cao' element={<HOC Component={ThongKe}></HOC>}></Route>
        <Route path='/thanh-toan' element={<HOC Component={ThanhToan}></HOC>}></Route>
        <Route path='/nhan-vien' element={<HOC Component={NhanSu}></HOC>}></Route>
        <Route path='/danh-muc' element={<HOC Component={DanhMuc}></HOC>}></Route>
        <Route path='/khuyen-mai' element={<HOC Component={KhuyenMai}></HOC>}></Route>
        <Route path='/bao-hanh' element={<HOC Component={BaoHanh}></HOC>}></Route>
        <Route path='/danh-gia' element={<HOC Component={DanhGia}></HOC>}></Route>
      </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
