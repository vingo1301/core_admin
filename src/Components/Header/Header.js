import React, { useState } from 'react'
import icon_home from '../../isset/img/icon/ICON-08.png'
import icon_logout from '../../isset/img/icon/ICON-14.png'
import { sessionService } from '../../services/sessionService/SessionService'
import './Header.css'
import { NavLink } from 'react-router-dom'

export default function Header() {
  let user = sessionService.getUser();
  let currentTab = window.location.pathname;
  let access_page = JSON.parse(user.access_page);
  let renderMenu = () => {
    let page = null;
    switch(currentTab){
      case "/app-tho": page =  2; break;
      case "/don-hang": page =  3; break;
      case "/dich-vu": page =  4; break;
      case "/quang-cao": page =  5; break;
      case "/thong-bao": page =  6; break;
      case "/bao-cao": page =  7; break;
      case "/nhan-vien": page =  8; break;
      case "/thanh-toan": page =  9; break;
      case "/danh-muc": page =  10; break;
      case "/khuyen-mai": page =  11; break;
      case "/bao-hanh": page =  12; break;
      case "/danh-gia": page =  13; break;
      default: page =  1;
    }
    return access_page.map((access_page) => {
      if(access_page.access_status == 1){
        if(access_page.access_id == page){
          return <div className='col-2 page'>
              <a className='active' href={`/${access_page.access_title}`}>{access_page.access_name}</a>
              </div>
          // return <li>
          //     <a className='active' href={`/${access_page.access_title}`}>{access_page.access_name}</a>
          //     </li>
        }else{
          return <div className='col-2 page'>
              <a href={`/${access_page.access_title}`}>{access_page.access_name}</a>
              </div>
          // return <li>
          //     <a href={`/${access_page.access_title}`}>{access_page.access_name}</a>
          //     </li>
        }
        
      }
    })
  }
  return (
    <div id='header' style={{background: "#037ec1"}}>
      <div className='headerTop py-2 px-3 d-flex justify-content-between'>
        <div className='headerLeft d-flex'>
          <div className=''>
            <p>PHẦN MỀM SỬA CHỮA NHÀ</p>
          </div>
          <div className='' style={{marginLeft:"10px"}}>
            <a href="/">
              <img width={"39px"} src={icon_home} alt="" />
            </a>
          </div>
        </div>
        <div className='headerRight'>
            <div>
              <NavLink onClick={sessionService.remove} to={"/"}>
                <img width={"39px"} src={icon_logout} alt="" />
              </NavLink>
            </div>
        </div>
      </div>
      <div style={{background:"#fff"}} className='headerBottom'>
        {/* <ul className='d-flex justify-content-around py-3'>
          {renderMenu()}
        </ul> */}
        <div className='row pt-3'>
          {renderMenu()}
        </div>
      </div>  
    </div>
  )
}
