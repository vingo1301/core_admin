import React from 'react'
import Header from '../Components/Header/Header'

 export default function HOC({Component}) {

  return (
    <div>
        <Header></Header>
        <Component></Component>
    </div>
  )
}
