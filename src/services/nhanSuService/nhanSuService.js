import axios from "axios";
import {BASE_URL} from '../configURL';

export const nhanSuService = {
  getUserList: (token) => {
    return axios({
      url: BASE_URL + "core/nhan-su/lay-nhan-vien",
      method: "GET",
      headers:{
        token
      }
    });
  },
  createUser: (token,data) => {
    return axios({
      url: BASE_URL + "core/nhan-su/tao-nhan-vien",
      method: "POST",
      data,
      headers:{
        token
      }
    });
  },
  updateUser: (token,data) => {
    return axios({
      url: BASE_URL + "core/nhan-su/sua-nhan-vien",
      method: "PUT",
      data,
      headers:{
        token
      }
    });
  }
};