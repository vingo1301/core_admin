import axios from "axios";
import {BASE_URL} from '../configURL';

export const khuyenMaiService = {
    getKhuyenMai: (token) => {
      return axios({
        url: BASE_URL + "core/khuyen-mai/lay-khuyen-mai",
        method: "GET",
        headers:{
          token
        }
      });
    },
    createKhuyenMai: (token,data) => {
        return axios({
          url: BASE_URL + `core/khuyen-mai/tao-khuyen-mai`,
          method: "POST",
          data: data,
          headers: { token: token },
        });
    },
    updateKhuyenMai: (token,data) => {
        return axios({
          url: BASE_URL + `core/khuyen-mai/sua-khuyen-mai`,
          method: "PUT",
          data: data,
          headers: { token: token },
        });
    },
    getDanhMuc: (token) => {
      return axios({
        url: BASE_URL + "app-khach/dich-vu/danh-sach-danh-muc",
        method: "GET",
        headers:{
          token
        }
      });
    }
  };