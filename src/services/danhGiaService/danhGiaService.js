import axios from "axios";
import {BASE_URL} from '../configURL';

export const danhGiaService = {
    getDanhGia: (token) => {
        return axios({
            url: BASE_URL + "core/danh-gia/lay-danh-gia",
            method: "GET",
            headers:{
              token
            }
          });
    }
}