import axios from "axios";
import {BASE_URL} from '../configURL';

export const danhMucService = {
    getDanhMuc: (token) => {
      return axios({
        url: BASE_URL + "core/danh-muc/lay-danh-muc",
        method: "GET",
        headers:{
          token
        }
      });
    },
    createDanhMuc: (token,data,file) => {
      const formData = new FormData();
      formData.append("danhmuc_hinhanh", file);
      formData.append("danhMuc",JSON.stringify(data));
      return axios({
        url: BASE_URL + `core/danh-muc/tao-danh-muc`,
        method: "POST",
        data: formData,
        headers: { token: token },
      });
    },
    updateDanhMuc: (token,data,file) => {
      const formData = new FormData();
      formData.append("danhmuc_hinhanh", file);
      formData.append("danhMuc",JSON.stringify(data));
      return axios({
        url: BASE_URL + `core/danh-muc/sua-danh-muc`,
        method: "PUT",
        data: formData,
        headers: { token: token },
      });
    }
  };