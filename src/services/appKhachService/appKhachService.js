import axios from "axios";
import {BASE_URL} from '../configURL';

export const appKhachService = {
  getUserList: (token) => {
    return axios({
      url: BASE_URL + "core/app-khach/user-list",
      method: "GET",
      headers:{
        token
      }
    });
  },
  createUser:(data,token) => {
    return axios({
      url: BASE_URL + "core/app-khach/tao-tai-khoan",
      method: "POST",
      data,
      headers:{
        token
      }
    });
  },
  updateUser:(data,token) => {
    return axios({
      url: BASE_URL + "core/app-khach/cap-nhat-tai-khoan",
      method: "PUT",
      data,
      headers:{
        token
      }
    });
  }
};