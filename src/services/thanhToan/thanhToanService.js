import axios from "axios";
import {BASE_URL} from '../configURL';

export const thanhToanService = {
    getDonHang: (token,date) => {
      return axios({
        url: BASE_URL + "core/don-hang/lay-don-da-thanh-toan/" + date,
        method: "GET",
        headers:{
          token
        }
      });
    },
    getThanhToan: (token) => {
      return axios({
        url: BASE_URL + "core/thanh-toan/phuong-thuc-thanh-toan",
        method: "GET",
        headers:{
          token
        }
      });
    },
    createThanhToan: (token,data,file) => {
      const formData = new FormData();
      formData.append("thanhtoan_img", file);
      formData.append("thanhToan",JSON.stringify(data));
      return axios({
        url: BASE_URL + `core/thanh-toan/tao-phuong-thuc-thanh-toan`,
        method: "POST",
        data: formData,
        headers: { token: token },
      });
    },
    updateThanhToan: (token,data,file) => {
      const formData = new FormData();
      formData.append("thanhtoan_img", file);
      formData.append("thanhToan",JSON.stringify(data));
      return axios({
        url: BASE_URL + `core/thanh-toan/sua-phuong-thuc-thanh-toan`,
        method: "PUT",
        data: formData,
        headers: { token: token },
      });
    }
  };
  