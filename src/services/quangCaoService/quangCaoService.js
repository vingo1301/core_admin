import axios from "axios";
import {BASE_URL} from '../configURL';

export const quangCaoService = {
    getQuangCao: (token) => {
      return axios({
        url: BASE_URL + "core/quang-cao/lay-quang-cao",
        method: "GET",
        headers:{
          token
        }
      });
    },
    createQuangCao: (data,token) => {
      return axios({
        url: BASE_URL + "core/quang-cao/tao-quang-cao",
        method: "POST",
        data,
        headers:{
          token
        }
      });
    },
    updateQuangCao: (data,token) => {
      return axios({
        url: BASE_URL + "core/quang-cao/sua-quang-cao",
        method: "PUT",
        data,
        headers:{
          token
        }
      });
    },
    uploadImg: (files,id,token) => {
      const formData = new FormData();
      formData.append("ads_img", files);
      return axios({
        url: BASE_URL + `core/quang-cao/upload-anh/${id}`,
        method: "POST",
        data: formData,
        headers: { token: token },
      });
    },
    getBanner: (token) => {
      return axios({
        url: BASE_URL + "core/quang-cao/lay-banner",
        method: "GET",
        headers:{
          token
        }
      });
    },
    updateBanner: (files,token) => {
      const formData = new FormData();
      formData.append("hinh_anh", files);
      return axios({
        url: BASE_URL + `core/quang-cao/update-banner`,
        method: "PUT",
        data: formData,
        headers: { token: token },
      });
    },
    editStatusBanner: (data,token) => {
      return axios({
        url: BASE_URL + "core/quang-cao/edit-banner",
        method: "PUT",
        data,
        headers:{
          token
        }
      });
    }
  };
  