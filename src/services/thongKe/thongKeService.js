import axios from "axios";
import {BASE_URL} from '../configURL';

export const thongKeService = {
    getDonHang: (token,day1,day2) => {
      return axios({
        url: BASE_URL + "core/don-hang/lay-don-da-hoan-thanh/?day1="+ day1 +"&day2=" + day2,
        method: "GET",
        headers:{
          token
        }
      });
    }
  };
  