import axios from "axios";
import {BASE_URL} from '../configURL';

export const donHangService = {
    getDonHang: (day1,day2,token) => {
      return axios({
        url: BASE_URL + "core/don-hang/lay-don-hang-theo-ngay/?day1="+day1 +"&day2="+day2,
        method: "GET",
        headers:{
          token
        }
      });
    },
    getDichVu:(token) => {
        return axios({
            url: BASE_URL + "core/dich-vu/lay-dich-vu-don-hang",
            method: "GET",
            headers:{
              token
            }
          });
    },
    createDonHang: (data,token) => {
      return axios({
        url: BASE_URL + "core/don-hang/tao-don-hang",
        method: "POST",
        data,
        headers:{
          token
        }
      });
    },
    updateDonHang: (data,token) => {
      return axios({
        url: BASE_URL + "core/don-hang/sua-don-hang",
        method: "PUT",
        data,
        headers:{
          token
        }
      });
    },
    deleteImage: (id,token) => {
        return axios({
          url: BASE_URL + "core/don-hang/xoa-hinh-anh/" + id,
          method: "DELETE",
          headers:{
            token
          }
        });
      },
    uploadImage: (files,id,token) => {
      const formData = new FormData();
      for(let key in files){
        formData.append("order_img", files[key]);
      }
      return axios({
        url: BASE_URL + "app-khach/don-hang/upload-anh/" + id,
        method: "POST",
        data: formData,
        headers: { token: token },
      });
    },
    checkMaDon: (token,order_code) => {
      return axios({
        url: BASE_URL + "core/don-hang/kiem-tra-ma-don/" + order_code,
        method: "GET",
        headers:{
          token
        }
      });
    },
    updateFile: (files,id,token) => {
      const formData = new FormData();
      formData.append("order_file", files);
      let donHang = {
        "order_id": id
      }
      formData.append("donHang",JSON.stringify(donHang));
      return axios({
        url: BASE_URL + "core/don-hang/update-file",
        method: "PUT",
        data: formData,
        headers: { token },
      });
    }
  };
  