import axios from "axios";
import {BASE_URL} from '../configURL';

export const appThoService = {
  getUserList: (token) => {
    return axios({
      url: BASE_URL + "core/app-tho/user-list",
      method: "GET",
      headers:{
        token
      }
    });
  },
  createUser:(data,token) => {
    return axios({
      url: BASE_URL + "core/app-tho/tao-tai-khoan",
      method: "POST",
      data,
      headers:{
        token
      }
    });
  },
  updateUser:(data,token) => {
    return axios({
      url: BASE_URL + "core/app-tho/cap-nhat-tai-khoan",
      method: "PUT",
      data,
      headers:{
        token
      }
    });
  },
  updateCCCD:(files1,files2,user_id,update_type,token) => {
    const formData = new FormData();
    let tho = {
      "tho_id": user_id,
      "update_type": update_type
    }
    formData.append("user_img",files1);
    formData.append("user_img",files2);
    formData.append("tho",JSON.stringify(tho));
    return axios({
      url: BASE_URL + "core/app-tho/cap-nhat-cccd",
      method: "PUT",
      data: formData,
      headers: { token },
    });
  },
  updateAvatar:(file,user_id,token) => {
    const formData = new FormData();
    let tho = {
      "tho_id": user_id
    }
    formData.append("user_avatar",file);
    formData.append("tho",JSON.stringify(tho));
    return axios({
      url: BASE_URL + "core/app-tho/cap-nhat-avatar",
      method: "PUT",
      data: formData,
      headers: { token },
    });
  }
};