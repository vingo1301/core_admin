import axios from "axios";
import {BASE_URL} from '../configURL';

export const baoHanhService = {
    getBaoHanh: (token,day1,day2) => {
      return axios({
        url: BASE_URL + "core/bao-hanh/lay-bao-hanh/?day1=" + day1 + "&day2=" + day2,
        method: "GET",
        headers:{
          token
        }
      });
    },
    updateBaoHanh: (data,token) => {
      return axios({
        url: BASE_URL + "core/bao-hanh/sua-bao-hanh",
        method: "PUT",
        data,
        headers:{
          token
        }
      });
    }
  };