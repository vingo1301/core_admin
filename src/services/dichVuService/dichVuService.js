import axios from "axios";
import {BASE_URL} from '../configURL';

export const dichVuService = {
    getDichVu: (token) => {
      return axios({
        url: BASE_URL + "core/dich-vu/lay-dich-vu",
        method: "GET",
        headers:{
          token
        }
      });
    },
    createDichVu: (data,token) => {
      return axios({
        url: BASE_URL + "core/dich-vu/tao-dich-vu",
        method: "POST",
        data,
        headers:{
          token
        }
      });
    },
    updateDichVu: (data,token) => {
      return axios({
        url: BASE_URL + "core/dich-vu/cap-nhat-dich-vu",
        method: "PUT",
        data,
        headers:{
          token
        }
      });
    },
    uploadImg: (files,id,token) => {
      const formData = new FormData();
      formData.append("service_img", files);
      return axios({
        url: BASE_URL + `core/dich-vu/upload-anh/${id}`,
        method: "POST",
        data: formData,
        headers: { token: token },
      });
    },
    getDanhMuc: (token) => {
      return axios({
        url: BASE_URL + "app-khach/dich-vu/danh-sach-danh-muc",
        method: "GET",
        headers:{
          token
        }
      });
    }
  };
  