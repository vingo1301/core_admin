import axios from "axios";
import {BASE_URL} from '../configURL';

export const userService = {
  getLogin: (userForm) => {
    return axios({
      url: BASE_URL + "core/user/login",
      method: "POST",
      data: {
        username: userForm.username,
        password: userForm.password,
      },
    });
  },
  getUserTho: (token) => {
    return axios({
      url: BASE_URL + "core/user/lay-user-tho",
      method: "GET",
      headers:{
        token
      }
    });
  },
  getUserKhach: (token) => {
    return axios({
      url: BASE_URL + "core/user/lay-user-khach",
      method: "GET",
      headers:{
        token
      }
    });
  }
};
