import axios from "axios";
import {BASE_URL} from '../configURL';

export const thongBaoService = {
    getThongBao: (token) => {
      return axios({
        url: BASE_URL + "core/thong-bao/lay-thong-bao",
        method: "GET",
        headers:{
          token
        }
      });
    },
    createThongBao: (data,token) => {
      return axios({
        url: BASE_URL + "core/thong-bao/tao-thong-bao",
        method: "POST",
        data,
        headers:{
          token
        }
      });
    },
    updateThongBao: (data,token) => {
      return axios({
        url: BASE_URL + "core/thong-bao/sua-thong-bao",
        method: "PUT",
        data,
        headers:{
          token
        }
      });
    }
  };
  