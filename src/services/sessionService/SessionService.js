
export const USERLOGIN = "USERLOGIN";
export const sessionService = {
  setUser: (user) => {
    let userJson = JSON.stringify(user);
    sessionStorage.setItem(USERLOGIN, userJson);
  },
  getUser: () => {
    let user = sessionStorage.getItem(USERLOGIN);
    if (user !== null) {
      return JSON.parse(user);
    } else {
      
      return null;
    }
  },
  remove: () => {
    sessionStorage.removeItem(USERLOGIN);
  },
};
